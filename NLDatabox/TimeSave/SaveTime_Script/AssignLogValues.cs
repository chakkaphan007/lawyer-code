using Databox;
using UnityEngine;

namespace Naplab.NLDatabox.TimeSave.SaveTime_Script
{
    public class AssignLogValues : MonoBehaviour
    {
        //public MeshRenderer meshRenderer;	
        DataboxObject database;
        string timeId;

        // year Value
        public void SetTimeToDataBox(string _timeEntry, DataboxObject _database)
        {
            timeId = _timeEntry;
            database = _database;
			
            var year = database.GetData<IntType>("Mission_1",timeId, "Year");
            year.Value = System.DateTime.Now.Year;
        
            var month = database.GetData<IntType>("Mission_1",timeId, "Month");
            month.Value = System.DateTime.Now.Month;
        
            var day  = database.GetData<IntType>("Mission_1",timeId, "Day");
            day.Value = System.DateTime.Now.Day;
        
            var hour = database.GetData<IntType>("Mission_1",timeId, "Hour");
            hour.Value = System.DateTime.Now.Hour;
        
            var minute = database.GetData<IntType>("Mission_1",timeId, "Minute");
            minute.Value = System.DateTime.Now.Minute;
        
            var second = database.GetData<IntType>("Mission_1",timeId, "Second");
            second.Value = System.DateTime.Now.Second;
        }
    
        //set GUID
        public void SetTime(string _timeEntry, DataboxObject _database)
        {
            timeId = _timeEntry;
            database = _database;
			
            var year = database.GetData<StringType>("Mission_Test",timeId, "Time");
            year.Value = System.DateTime.Now.ToString();

        }
    }
}
