using System.Collections.Generic;
using Databox;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLDatabox.TimeSave.SaveTime_Script
{
    public class DataboxSaveLink : MonoBehaviour
    {
        public DataboxObjectManager Manager;
        private DataboxObject Mission;
    
        [SerializeField] private GameObject TextPrefab;
        [SerializeField] private Transform TimePanel;
    
        [SerializeField] private List<string> TimeList = new List<string>();
        private void Start()
        {
            Mission = Manager.GetDataboxObject("Mission");
        }
    
        public void LinkData(string _Mission)
        { 
            //Destroy child Before Instantiate
            foreach (Transform child in TimePanel.transform) {
                Destroy(child.gameObject);
            }
            TimeList.Clear();
        
            foreach (var entriesKey in Mission.DB["Mission_Test"].entries.Keys)
            {
                var year = Mission.GetData<StringType>("Mission_Test",entriesKey,"Time");
                //var month = Mission.GetData<IntType>("Mission_1",entriesKey,"Month");
                TimeList.Add(year.Value);
                //TimeList.Add(month.Value);

                GameObject textgaGameObject = Instantiate(TextPrefab);
                textgaGameObject.transform.SetParent(TimePanel);

                var text = textgaGameObject.GetComponentInChildren<Text>();
                text.text = year.Value.ToString() + " - " + _Mission;
            }
        }
    
    }
}
