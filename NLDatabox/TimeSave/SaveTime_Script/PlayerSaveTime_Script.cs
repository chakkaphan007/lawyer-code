using System;
using Databox;
using UnityEngine;

namespace Naplab.NLDatabox.TimeSave.SaveTime_Script
{
    public class PlayerSaveTime_Script : MonoBehaviour
    {
        public AssignLogValues _assignLogValues;
        public DataboxObjectManager Manager;
        private DataboxObject initialData;
        private DataboxObject Mission;
        public DataboxSaveLink _databoxSaveLink;

        void OnEnable()
        {
            Manager.LoadAll();
        }

        private void OnDisable()
        {
            Mission.SaveDatabase();
        }

        private void Start()
        {
            Mission = Manager.GetDataboxObject("Mission");
            initialData = Manager.GetDataboxObject("Initial");
        }

        public void SaveMission_1()
        {
            Guid g = Guid.NewGuid();

            initialData.RegisterToDatabase(Mission, "Mission_Test",
                "Time", g.ToString());

            //_assignLogValues.SetTimeToDataBox(g.ToString(), Mission);
            _assignLogValues.SetTime(g.ToString(), Mission);
        
            _databoxSaveLink.LinkData("Mission_1");
        }
        public void SaveMission_2()
        {
            Guid g = Guid.NewGuid();

            initialData.RegisterToDatabase(Mission, "Mission_2",
                "Time", g.ToString());

            _assignLogValues.SetTimeToDataBox(g.ToString(), Mission);
            //_databoxSaveLink.LinkData("Mission_2");
        }
        public void SaveMission_3()
        {
            Guid g = Guid.NewGuid();

            initialData.RegisterToDatabase(Mission, "Mission_3",
                "Time", g.ToString());

            _assignLogValues.SetTimeToDataBox(g.ToString(), Mission);
            //_databoxSaveLink.LinkData("Mission_2");
        }
    }
}
