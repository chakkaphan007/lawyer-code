using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using Naplab.NLUtils.Scripts.Test;
using UnityEngine;
using UnityEngine.UI;

public class AnalyzeLoad : NLDataBox
{
    public List<GameObject> _Itemlist;
    

    public void LoadAnalyze()
    {

         foreach (Transform child in gameObject.transform) {
             Destroy(child.gameObject);
         }
         _Itemlist.Clear();
         foreach (var entriesKey in ItemManager.GetDataboxObject(ItemSave).DB[Itemlist].entries.Keys)
         {
             var _Added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist, entriesKey, Added);
             if (_Added.Value)
             {
                 var item = ItemManager.GetDataboxObject(ItemSave).GetData<ResourceType>(Itemlist, entriesKey, "Prefab")
                     .Load() as GameObject;
                 var _slotindex = ItemManager.GetDataboxObject(ItemSave)
                     .GetData<IntType>(Itemlist, entriesKey, "SlotIndex");

                 if (item == null) continue;
                 item.GetComponentInChildren<Call_Info>()._ItemState = ItemState.InSlot;
                 item.GetComponent<Button>().interactable = true;
                 _Itemlist.Add(item);
             }
         }

         StartCoroutine(SortItem(_Itemlist));
    }
    
    IEnumerator SortItem(List<GameObject> goItemlist)
            {
                // //sort number of Item
                // for (int one = 0; one < goItemlist.Count; one++)
                // {
                //     var firstIndex = one;
                //
                //     for (int two = one + 1; two < goItemlist.Count; two++)
                //     {
                //         if (goItemlist[one].GetComponent<Call_Info>()._Itemindex 
                //             > goItemlist[two].GetComponent<Call_Info>()._Itemindex)
                //         {
                //             firstIndex = two;
                //         }
                //     }
                //
                //     if (firstIndex != one)
                //     {
                //         (goItemlist[one], goItemlist[firstIndex]) = (goItemlist[firstIndex], goItemlist[one]);
                //     }

                    foreach (Transform child in gameObject.transform)
                    {
                        Destroy(child.gameObject);
                    }

                    foreach (var t in _Itemlist)
                    {
                        GameObject instantiate = Instantiate(t, gameObject.transform);
                        instantiate.name = t.name;
                        //Rescale to fit slot
                        instantiate.transform.Find("Text_Name").gameObject.SetActive(false);
                        instantiate.GetComponent<RectTransform>().sizeDelta = new Vector2(214.33f, 228.66f);

                        var imgicon = instantiate.transform.Find("Image_Icon").GetComponent<RectTransform>();
                        imgicon.anchorMax = new Vector2(0.5f, 0.5f);
                        imgicon.anchorMin = new Vector2(0.5f, 0.5f);
                        imgicon.pivot = new Vector2(0.5f, 0.5f);
                        imgicon.rotation = new Quaternion(0, 0, 0, 0);
                        imgicon.anchoredPosition = new Vector2(0, 0);
                        imgicon.localScale = new Vector3(1.3f, 1.3f, 1.3f);

                        instantiate.transform.Find("Image_BG").GetComponent<RectTransform>().anchoredPosition =
                            new Vector2(-61, 18);

                        instantiate.transform.Find("Butt_Remove").gameObject.SetActive(true);
                        //Debug.Log(instantiate.name);
                    }

                    yield return null;
            }

    //Instantiate Item
}
