using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using UnityEngine;

namespace Naplab.NLDatabox.Inventory.DataBox_Inventory
{
    public class AddAndRemove : NLDataBox
    {
        public string EntryItem;
        public int _SlotNumber;
        
        
        public ItemLoad _ItemLoad;
        private AnalyzeLoad _AnswerLoad;
        private Analyze _analyze;
        private BoolType _Added;
        private IntType _slotindex;
        private IntType _slotCount;

        private void Start()
        {
            _analyze = FindObjectOfType<Analyze>();
            _ItemLoad = FindObjectOfType<ItemLoad>();
            _AnswerLoad = FindObjectOfType<AnalyzeLoad>();
            _Added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist, EntryItem,Added );
            _slotindex = ItemManager.GetDataboxObject(ItemSave).GetData<IntType>(Itemlist, EntryItem,"SlotIndex" );
            _slotCount = globalManager.GetDataboxObject(RunTimeDataBox).GetData<IntType>(MissionData, "Part2","SlotCount" );
        }

        public void MoveItem()
        {
            _Added.Value = true;
            
            // if (_ItemLoad._LoadState == LoadState.Conversation)
            // {
            //     //Debug.Log("load");
            //     _ItemLoad.loadConversation();
            // }
            // if (_ItemLoad._LoadState == LoadState.Evidence)
            // {
            //     //Debug.Log("load");
            //     _ItemLoad.loadEvidence();
            // }
            // if (_ItemLoad._LoadState == LoadState.Summary)
            // {
            //     Debug.Log("load");
            //     _ItemLoad.loadSummary();
            // }

            // _slotCount.Value += 1;
            // _slotindex.Value = _slotCount.Value;
            _AnswerLoad.LoadAnalyze();
            SaveGame();
            
        }

        public void Remove()
        {
            _Added.Value = false;

            if (_ItemLoad._LoadState == LoadState.Conversation)
            {
                _ItemLoad.loadConversation();
            }
            if (_ItemLoad._LoadState == LoadState.Evidence)
            {
                _ItemLoad.loadEvidence();
            }
            _slotCount.Value -= 1;
            SaveGame();
            _slotindex.Value = 99;
            _AnswerLoad.LoadAnalyze();
        }
    }
}
