using System;
using System.Collections;
using System.Collections.Generic;
using Fungus;
using Naplab.NLDatabox.Script;
using Sirenix.Utilities;
using UnityEngine;

public class PhoneContact : MonoBehaviour
{
    public String ContactState;
    public DataboxManager_Gameplay _Databox;
    public void SendToFlowChart()
    {
        //เช็คว่า energy พอไหม
        if (_Databox._EnergyBar.currentPercent > 0)
        {
            //เช็คว่าเลือก mission ไปรึยัง
            if (ContactState.IsNullOrWhitespace())
            {
                Flowchart.BroadcastFungusMessage("SelectMissionNotice");
                Debug.Log("VAR");   
            }
            else
            {
                Flowchart.BroadcastFungusMessage(ContactState);
            }
        }
        else
        {
            Flowchart.BroadcastFungusMessage("NotEnoughEnergy");
        }
    }
}
