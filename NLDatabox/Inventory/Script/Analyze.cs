using System.Collections.Generic;
using System;
using Fungus;
using Michsky.UI.ModernUIPack;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using Naplab.NLUtils.Scripts.Menu_Script;
using Naplab.NLUtils.Scripts.Score_Scrpt;
using UnityEngine;


public class Analyze : NLDataBox
{
    public AnalyzePoint Quest;
    public ProgressBar PotentialBar;
    private DataboxManager_Gameplay _databoxManagerGameplay;
    public PartTwoScore _PartTwoScore;
    public PopupTime _Popup;
    private GiveUp _giveUp;

    public List<GameObject> ChoiceButton = new List<GameObject>();
    

    private void Start()
    {
        _databoxManagerGameplay = FindObjectOfType<DataboxManager_Gameplay>();
        _giveUp = FindObjectOfType<GiveUp>();
    }

    public void Analysis()
    {
        int TotalNegativeEnergy = 0;
        int TotalPotential = 0;

        PotentialBar.currentPercent = 0;

        TotalNegativeEnergy = (int) _databoxManagerGameplay._EnergyBar.currentPercent;
        foreach (var entriesKey in ItemManager.GetDataboxObject(ItemSave).DB[Itemlist].entries.Keys)
        {
            var _Added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist, entriesKey, Added);
            if (_Added.Value)
            {
                var Potential = ItemManager.GetDataboxObject(ItemSave)
                    .GetData<StringListType>(Itemlist, entriesKey, "Potential");
                var Point = ItemManager.GetDataboxObject(ItemSave).GetData<IntType>(Itemlist, entriesKey, "Point");
                var IsMatch = Potential.Value.Find(s => s == Quest.ToString());
                if (IsMatch == Quest.ToString() && _databoxManagerGameplay._EnergyBar.currentPercent > 0)
                {
                    TotalPotential += Point.Value;
                    TotalNegativeEnergy -= 1;
                    Debug.Log(TotalPotential);
                }
                else
                {
                    TotalNegativeEnergy -= 1;
                }
            }
        }

        if (TotalNegativeEnergy < 0)
        {
            Debug.Log("Cant");
            Flowchart.BroadcastFungusMessage("NotEnoughEnergy");
        }
        else
        {
            _databoxManagerGameplay._Energy.Value = TotalNegativeEnergy;
            _databoxManagerGameplay._EnergyBar.currentPercent = _databoxManagerGameplay._Energy.Value;
            PotentialBar.currentPercent = TotalPotential;
            //Debug.Log(TotalPotential);
            //PotentialBar.currentPercent = 100;
             if (PotentialBar.currentPercent >= 100)
             {
                 switch (Quest)
                 {
                     case AnalyzePoint.Tes:
                         Flowchart.BroadcastFungusMessage("PassMission1");
                         _Popup._saveMission1.Value = false;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission1Pass();
                         break;
                     case AnalyzePoint.CA:
                         Flowchart.BroadcastFungusMessage("PassMission2");
                         _Popup._saveMission2.Value = false;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission2Pass();
                         break;
                     case AnalyzePoint.HA:
                         Flowchart.BroadcastFungusMessage("PassMission3");
                         _Popup._saveMission3.Value = false;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission3Pass();
                         break;
                     case AnalyzePoint.DOD:
                         Flowchart.BroadcastFungusMessage("PassMission4");
                         _Popup._saveMission4.Value = false;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission4Pass();
                         break;
                     case AnalyzePoint.L:
                         Flowchart.BroadcastFungusMessage("PassMission5");
                         _Popup._saveMission5.Value = false;
                         _Popup._savePassedMission5.Value = true;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission5Pass();
                         break;
                     case AnalyzePoint.QM:
                         Flowchart.BroadcastFungusMessage("PassMission6");
                         _Popup._saveMission6.Value = false;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission6Pass();
                         break;
                     case AnalyzePoint.H:
                         Flowchart.BroadcastFungusMessage("PassMission7");
                         _Popup._saveMission7.Value = false;
                         _Popup._savePassedMission7.Value = true;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission7Pass();
                         break;
                     case AnalyzePoint.S1:
                         Flowchart.BroadcastFungusMessage("PassMission8");
                         _Popup._saveMission8.Value = false;
                         _Popup._savePassedMission8.Value = true;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission8Pass();
                         break;
                     case AnalyzePoint.S2:
                         Flowchart.BroadcastFungusMessage("PassMission9");
                         _Popup._saveMission9.Value = false;
                         _Popup._savePassedMission9.Value = true;
                         _Popup.SyncMissionInteract();
                         _giveUp.giveup();
                         _PartTwoScore.Mission9Pass();
                         break;
                 }
             }
             if (PotentialBar.currentPercent < 100)
             {
                 switch (Quest)
                 {
                     case AnalyzePoint.Tes:
                         Flowchart.BroadcastFungusMessage("NotPassMission1");
                         _PartTwoScore.Mission1Fail();
                         break;
                     case AnalyzePoint.CA:
                         Flowchart.BroadcastFungusMessage("NotPassMission2");
                         _PartTwoScore.Mission2Fail();
                         break;
                     case AnalyzePoint.HA:
                         Flowchart.BroadcastFungusMessage("NotPassMission3");
                         _PartTwoScore.Mission3Fail();
                         break;
                     case AnalyzePoint.DOD:
                         Flowchart.BroadcastFungusMessage("NotPassMission4");
                         _PartTwoScore.Mission4Fail();
                         break;
                     case AnalyzePoint.L:
                         Flowchart.BroadcastFungusMessage("NotPassMission5");
                         _PartTwoScore.Mission5Fail();
                         break;
                     case AnalyzePoint.QM:
                         break;
                     case AnalyzePoint.H:
                         Flowchart.BroadcastFungusMessage("NotPassMission7");
                         _PartTwoScore.Mission7Fail();
                         break;
                     case AnalyzePoint.S1:
                         Flowchart.BroadcastFungusMessage("NotPassMission8");
                         _PartTwoScore.Mission8Fail();
                         break;
                     case AnalyzePoint.S2:
                         Flowchart.BroadcastFungusMessage("NotPassMission9");
                         _PartTwoScore.Mission9Fail();
                         break;
                 }
                 SaveGame();
             }
        }
        
    }
}