using UnityEngine;

namespace Naplab.NLDatabox.Inventory.DataBox_Inventory
{
    public class ClearItem : MonoBehaviour
    {
        public GameObject PanelInfo;

        public void Clear()
        {
            foreach (Transform child in PanelInfo.transform) {
                Destroy(child.gameObject);
            }
        }
    }
}
