using UnityEngine;

namespace Naplab.NLDatabox.Inventory.Script
{
    public enum LoadState{ Conversation, Evidence, Summary}
    public enum ItemState{ Analysis,Inventory,InSlot,Instance }
    public enum ContactState {Testament,ClientAgreement,HeirAgreement,Dateofdeath,Legacy,QualificationOfManager,Heir,SP1,SP2}
    public enum AnalyzePoint{Tes,CA,HA,DOD,L,QM,H,S1,S2}
}
