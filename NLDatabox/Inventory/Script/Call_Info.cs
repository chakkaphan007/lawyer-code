using System;
using MalbersAnimations.Events;
using Naplab.NLDatabox.Inventory.DataBox_Inventory;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Test
{
    public class Call_Info : NLDataBox
    {
        
        private GameObject InfoPos;
        private GameObject PopupPos;
        private GameObject ItemImage;

        private GameObject IconImage;

       [HideInInspector] public string entryId;
        
        public ItemState _ItemState;
        private AddAndRemove _addAndRemove;
        
        public int _Itemindex;
        private GameObject _PanelE;
        private GameObject _PanelC;
        
        private GameObject _go;

        private void Start()
        {
            gameObject.transform.position = Vector3.back;
            string[] newName = gameObject.name.Split('(');
            //Debug.Log(newName[0]);
            gameObject.name = newName[0];
            entryId = newName[0];
            
            //รับข้อมูลจาก databox จาก itemOrigin ลง itemSave
            ItemManager.GetDataboxObject(ItemOrigin).RegisterToDatabase(ItemManager.GetDataboxObject(ItemSave),Itemlist, entryId, entryId);
            ItemManager.GetDataboxObject(ItemSave).SaveDatabase();
            InfoPos = GameObject.Find("InfoPos");
            PopupPos = GameObject.Find("Canvas_Pop");

            //ให้ child ที่มี NLDataboxBlinding รับค่า entryid จาก object นี้ไป
            var databoxUIBinding = GetComponentsInChildren<NLDataBoxBlinding>();
            foreach (var EntryID in databoxUIBinding)
            {
                EntryID.entryID = entryId;
            }
            
            _addAndRemove = FindObjectOfType<AddAndRemove>();
            ItemImage = gameObject.transform.Find("Image_Icon").gameObject;
            IconImage = gameObject.transform.Find("Icon_Image").gameObject;
            
            _PanelC = GameObject.Find("Panel_Analysis_C");
            _PanelE = GameObject.Find("Panel_Analysis_E");
            if (_ItemState == ItemState.Instance)
            {
                PopupItem();
                if (_PanelC == null||_PanelE == null)
                {
                    return;
                }
                _PanelC.GetComponent<ItemLoad>().loadConversation();
                _PanelE.GetComponent<ItemLoad>().loadEvidence();
            }
        }

        private void Update()
        {
            var added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist,entryId,Added);
            if (added.Value &&_ItemState != ItemState.InSlot && _ItemState != ItemState.Inventory)
            {
                gameObject.GetComponent<Button>().interactable = false;
            }
            if (!added.Value)
            {
                gameObject.GetComponent<Button>().interactable = true;
            }
        }

        public void CallInfo()
        {
            //ลบ child  ทั้งหมดออกจาก infoPos
            foreach (Transform child in InfoPos.transform) 
            {
                Destroy(child.gameObject);
            }

            
            if (_ItemState == ItemState.Analysis || _ItemState == ItemState.InSlot)
            {
                //ส่งข้อมูล item ไปแสดง
                SetInfoText();
                var _AR = FindObjectsOfType<AddAndRemove>();
                foreach (var addAndRemove in _AR)
                {
                    addAndRemove.EntryItem = entryId;
                }
            }

            if (_ItemState == ItemState.Inventory)
            {
                SetInfoText();
            }
        }

        public void SetInfoText()
        {
            //เลือกหน้าต่างที่จะแสดงผล
            _go = _ItemState switch
            {
                ItemState.Analysis => Instantiate(Resources.Load("ImageInfoEvidence") as GameObject, InfoPos.transform),
                ItemState.InSlot => Instantiate(Resources.Load("ImageInfoRemove") as GameObject, InfoPos.transform),
                ItemState.Inventory => Instantiate(Resources.Load("ImageInventory") as GameObject, InfoPos.transform),
                _ => _go
            };

            //รับข้อมูลจาก databox และset ข้อมูลลงใน prefabs
            var _Name = _go.gameObject.transform.Find("ItemName").GetComponent<TMP_Text>();
            var _Detail = _go.gameObject.transform.Find("Detail").GetComponent<TMP_Text>();
            var _NPC_Name = _go.gameObject.transform.Find("NPC_Name").GetComponent<TMP_Text>();
            var _Relationship = _go.gameObject.transform.Find("Relationship").GetComponent<TMP_Text>();
            var _ImageItem = _go.gameObject.transform.Find("ImageItem");
            var _IconImage = _go.gameObject.transform.Find("Icon_Image");
            
            var DataItemName = ItemManager.GetDataboxObject(ItemSave).GetData<StringType>(Itemlist, entryId, ItemName);
            var DataItemDetail = ItemManager.GetDataboxObject(ItemSave).GetData<StringType>(Itemlist, entryId, ItemDetail);
            var DataRelationship = ItemManager.GetDataboxObject(ItemSave).GetData<StringType>(Itemlist, entryId, Relationship);
            var DataNPC_Name = ItemManager.GetDataboxObject(ItemSave).GetData<StringType>(Itemlist, entryId, NPC_Name);
            //var DataImage = ItemManager.GetDataboxObject(ItemSave).GetData<ResourceType>(Itemlist, entryId, "Image").Load() as Texture2D;
            
            //set ขนาดรูป
            var imageInstantiate = Instantiate(ItemImage, _ImageItem.transform).GetComponent<RectTransform>();
            imageInstantiate.localScale = new Vector3(1.3f, 1.3f);
            imageInstantiate.rotation = new Quaternion(0, 0, 0, 0);
            imageInstantiate.anchoredPosition = new Vector2(0, 0);
            
            var IconInstantiate = Instantiate(IconImage ,_IconImage.transform).GetComponent<RectTransform>();
            IconInstantiate.localScale = new Vector3(1.3f, 1.3f);
            IconInstantiate.rotation = new Quaternion(0, 0, 0, 0);
            IconInstantiate.anchoredPosition = new Vector2(0, 0);
            IconInstantiate.GetComponent<Image>().color = new Color(255, 255, 255, 1);
            _Name.text = DataItemName.Value;
            _Detail.text = DataItemDetail.Value;
            _NPC_Name.text = DataNPC_Name.Value;
            _Relationship.text = DataRelationship.Value;
        }

        public void PopupItem()
        {
            _ItemState = ItemState.Analysis;
            
            //รับข้อมูลจาก databox และset ข้อมูลลงใน prefabs
            var DataItemName = ItemManager.GetDataboxObject(ItemSave).GetData<StringType>(Itemlist, entryId, ItemName);
            var popItem = Instantiate(Resources.Load("Item_PopUp") as GameObject, PopupPos.transform);
            var Image_Item =popItem.transform.Find("Image_Item");
            
            //set รูปตามขนาดที่กำหนด
            var imageInstantiate = Instantiate(ItemImage, Image_Item.transform).GetComponent<RectTransform>();
            imageInstantiate.GetComponent<Image>().SetNativeSize();
            imageInstantiate.localScale = new Vector3(2.2f, 2.2f);
            imageInstantiate.rotation = new Quaternion(0, 0, 0, 0);
            imageInstantiate.anchoredPosition = new Vector2(0, 0);
            imageInstantiate.anchorMax = new Vector2(0.5f, 0.5f);
            imageInstantiate.anchorMin = new Vector2(0.5f, 0.5f);
            imageInstantiate.pivot = new Vector2(0.5f, 0.5f);
            
            var _Name = popItem.gameObject.transform.Find("Name").GetComponent<TMP_Text>();
            _Name.text = DataItemName.Value;
        }
    }
}
