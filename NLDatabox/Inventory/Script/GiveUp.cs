using System;
using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Inventory.DataBox_Inventory;
using Naplab.NLDatabox.Script;
using UnityEngine;

public class GiveUp : NLDataBox
{
    private ItemLoad _itemLoad;
    private Analyze _analyze;
    private IntType _slotCount;

    private void Start()
    {
        _itemLoad = FindObjectOfType<ItemLoad>();
        _analyze = FindObjectOfType<Analyze>();
        _slotCount = globalManager.GetDataboxObject(RunTimeDataBox).GetData<IntType>(MissionData, "Part2","SlotCount" );
    }

    public void giveup()
    {
        //ลบ item ออกจาก object ทั้งหมด
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        
        //ลบสถานะถูกเรียกเข้า analyze
        foreach (var entriesKey in ItemManager.GetDataboxObject(ItemSave).DB[Itemlist].entries.Keys)
        {
            var _Added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist, entriesKey, Added);
            if (_Added.Value)
            {
                _Added.Value = false;
            }
        }
        _analyze.PotentialBar.currentPercent = 0;
        _slotCount.Value = 0;
        SaveGame();
    }
}
