using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Inventory.DataBox_Inventory;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using Naplab.NLUtils.Scripts.Test;
using UnityEngine;
using UnityEngine.Serialization;

public class RemoveItem : NLDataBox
{
    public string EntryID;
    public int _SlotNumber;
    public ItemLoad _ItemLoad;
    private AnalyzeLoad _AnswerLoad;
    private Analyze _analyze;
    private BoolType _Added;
    private IntType _slotindex;
    private IntType _slotCount;
    void Start()
    {
        Call_Info _call = GetComponentInParent<Call_Info>();
        EntryID = _call.entryId;
        
        _analyze = FindObjectOfType<Analyze>();
        _ItemLoad = FindObjectOfType<ItemLoad>();
        _AnswerLoad = FindObjectOfType<AnalyzeLoad>();
        _Added = ItemManager.GetDataboxObject(ItemSave).GetData<BoolType>(Itemlist, EntryID,Added );
        _slotindex = ItemManager.GetDataboxObject(ItemSave).GetData<IntType>(Itemlist, EntryID,"SlotIndex" );
        _slotCount = globalManager.GetDataboxObject(RunTimeDataBox).GetData<IntType>(MissionData, "Part2","SlotCount" );
    }
    public void Remove()
    {
        //ให้ออกจากสถานะ analyze และเรียกโหลดอีกครั้ง
        _Added.Value = false;

        if (_ItemLoad._LoadState == LoadState.Conversation)
        {
            _ItemLoad.loadConversation();
        }
        if (_ItemLoad._LoadState == LoadState.Evidence)
        {
            _ItemLoad.loadEvidence();
        }
        
        _ItemLoad.sortItem(_ItemLoad._Itemlist);
        _slotCount.Value -= 1;
        SaveGame();
        _slotindex.Value = 99;
        _AnswerLoad.LoadAnalyze();
    }
}
