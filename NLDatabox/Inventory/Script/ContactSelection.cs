using System;
using System.Collections;
using System.Collections.Generic;
using Fungus;
using Naplab.NLDatabox.Inventory.Script;
using UnityEngine;

public class ContactSelection : MonoBehaviour
{
    public ContactState _ContactSelection;

    public void SetContact()
    {
        //เช็คว่าตอนนี้อยู่ mission ไหน (part 2)
        //และใช้ส่งไปที่ flowchart ด้วย
        FindObjectOfType<PhoneContact>().ContactState = _ContactSelection.ToString();
    }
}
