using System;
using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Inventory.Script;
using UnityEngine;

public class SelectPoint : MonoBehaviour
{
    public AnalyzePoint Point;
    
    public void SetPoint()
    {
        //เลือกว่าอยู่ mission ไหน
        FindObjectOfType<Analyze>().Quest = Point;
    }
}
