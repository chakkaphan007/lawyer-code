using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Fungus;
using MalbersAnimations.Events;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLDatabox.Script;
using Naplab.NLUtils.Scripts.Test;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLDatabox.Inventory.DataBox_Inventory
{
    public class ItemLoad : NLDataBox
    {
        public ItemState ItemState;
        public LoadState _LoadState;
        public List<GameObject> _Itemlist;

        private void Start()
        {
            _LoadState = LoadState.Evidence;
        }

        public void loadEvidence()
        {
            _LoadState = LoadState.Evidence;
            LoadItem(Evidence);
            StartCoroutine(sortItem(_Itemlist));
        }

        public void loadConversation()
        {
            _LoadState = LoadState.Conversation;
            LoadItem(Conversation);
            StartCoroutine(sortItem(_Itemlist));
        }

        public void loadSummary()
        {
            _LoadState = LoadState.Summary;
            LoadItem("Summary");
            StartCoroutine(sortItem(_Itemlist));
        }

        
        public void LoadItem(string LoadData)
        {
            //ลบ item ใน slot ทั้งหมด
            foreach (Transform child in gameObject.transform)
            {
                Destroy(child.gameObject);
            }
            
            _Itemlist.Clear();

            //เรียก item จาก databox
            foreach (var entriesKey in ItemManager.GetDataboxObject(ItemSave).DB[Itemlist].entries.Keys)
            {
                var _Added = ItemManager.GetDataboxObject(ItemSave)
                    .GetData<BoolType>(Itemlist, entriesKey, Added);
                var _LoadData = ItemManager.GetDataboxObject(ItemSave)
                    .GetData<BoolType>(Itemlist, entriesKey, LoadData);
                // var _slotindex = ItemManager.GetDataboxObject(ItemSave)
                //     .GetData<IntType>(Itemlist, entriesKey, "SlotIndex");

                //เช็คว่าถูกเรียกจาก state ไหน
                if (_LoadData.Value && ItemState != ItemState.Inventory)
                {
                   //เรียก item จาก databox
                    var item = ItemManager.GetDataboxObject(ItemSave)
                        .GetData<ResourceType>(Itemlist, entriesKey, "Prefab").Load() as GameObject;
                    //Debug.Log(item.name);

                    var itemInFo = item.GetComponent<Call_Info>();
                    //var itemButt = item.GetComponent<Button>();
                    itemInFo._ItemState = ItemState.Analysis;
                    
                    //เช็ควว่าไอเทมไหนกำลังถูก analyze อยู่
                    if (_Added.Value)
                    {
                        //itemInFo._Itemindex = 99;
                        //itemInFo._Itemindex = _slotindex.Value;
                        //itemButt.interactable = false;
                        _Itemlist.Add(item);
                    }
                    else
                    {
                        //itemInFo._Itemindex = 99;
                        //itemInFo._Itemindex = _slotindex.Value;
                        //itemButt.interactable = true;
                        _Itemlist.Add(item);
                    }
                    //GameObject OB = Instantiate(item, gameObject.transform);
                    //sOB.name = item.name;
                }
                
                if (ItemState == ItemState.Inventory && _LoadData.Value)
                {
                    var item = ItemManager.GetDataboxObject(ItemSave)
                        .GetData<ResourceType>(Itemlist, entriesKey, "Prefab").Load() as GameObject;
                    item.GetComponentInChildren<Call_Info>()._ItemState = ItemState.Inventory;
                    GameObject OB = Instantiate(item, gameObject.transform);
                    OB.name = item.name;
                }
            }
        }

        public IEnumerator sortItem(List<GameObject> GoItemlist)
            {
            // int FirstIndex;
            // GameObject items;
            //
            // //sort interact Butt
            // for (int One = 0; One < GoItemlist.Count; One++)
            // {
            //     FirstIndex = One;
            //     for (int Two = One + 1; Two < GoItemlist.Count; Two++)
            //     {
            //         if (!GoItemlist[Two].GetComponent<Button>().interactable)
            //         {
            //             FirstIndex = Two;
            //         }
            //     }
            //     if (FirstIndex != One)
            //     {
            //         items = GoItemlist[One];
            //         GoItemlist[One] = GoItemlist[FirstIndex];
            //         GoItemlist[FirstIndex] = items;
            //     }
            // }
            //
            // //sort NonInteractable Item 
            // for (int One = 0; One < GoItemlist.Count; One++)
            // {
            //     FirstIndex = One;
            //     for (int Two = One + 1; Two < GoItemlist.Count; Two++)
            //     {
            //         if (!GoItemlist[Two].GetComponent<Button>().interactable&&
            //             (GoItemlist[One].GetComponent<Call_Info>()._Itemindex > GoItemlist[Two].GetComponent<Call_Info>()._Itemindex))
            //         {
            //             FirstIndex = Two;
            //         }
            //     }
            //     if (FirstIndex != One)
            //     {
            //         items = GoItemlist[One];
            //         GoItemlist[One] = GoItemlist[FirstIndex];
            //         GoItemlist[FirstIndex] = items;
            //     }
            // }
            //
            // //sort ItemIndex & Reset Number
            // for (int i = 0; i < GoItemlist.Count; i++)
            // {
            //     if (GoItemlist[i].GetComponent<Button>().interactable == false)
            //     {
            //         GoItemlist[i].GetComponent<Call_Info>()._Itemindex = i+1;
            //         var _slotindex = ItemManager.GetDataboxObject(ItemSave).GetData<IntType>
            //             (Itemlist, GoItemlist[i].GetComponent<Call_Info>().entryId,"SlotIndex" );
            //         _slotindex.Value = GoItemlist[i].GetComponent<Call_Info>()._Itemindex;
            //         SaveGame();
            //         //Debug.Log(_slotindex.Value + ":"+GoItemlist[i].name);
            //     }
            //
            //     if (GoItemlist[i].GetComponent<Button>().interactable)
            //     {
            //         GoItemlist[i].GetComponent<Call_Info>()._Itemindex = 99;
            //         var _slotindex = ItemManager.GetDataboxObject(ItemSave).GetData<IntType>
            //             (Itemlist, GoItemlist[i].GetComponent<Call_Info>().entryId,"SlotIndex" );
            //         _slotindex.Value = GoItemlist[i].GetComponent<Call_Info>()._Itemindex;
            //         SaveGame();
            //         //Debug.Log(_slotindex.Value + ":"+GoItemlist[i].name);
            //     }
            // }
            //
            // //sort number of Item
            // for (int One = 0; One < GoItemlist.Count; One++)
            // {
            //     FirstIndex = One;
            //     for (int Two = One + 1; Two < GoItemlist.Count; Two++)
            //     {
            //         if (GoItemlist[Two].GetComponent<Call_Info>()._Itemindex < GoItemlist[One].GetComponent<Call_Info>()._Itemindex)
            //         {
            //             FirstIndex = Two;
            //         }
            //     }
            //     if (FirstIndex != One)
            //     {
            //         items = GoItemlist[One];
            //         GoItemlist[One] = GoItemlist[FirstIndex];
            //         GoItemlist[FirstIndex] = items;
            //     }
            // }

            //Instantiate Item
            foreach (var t in _Itemlist)
            {
               GameObject OB = Instantiate(t,gameObject.transform);
               //OB.name = t.name;
               yield return new WaitForSeconds(0.5f);
            }
            yield return null;
        }
    }
}
