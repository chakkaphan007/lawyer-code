using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Databox;
using JetBrains.Annotations;
using MalbersAnimations.Scriptables;
using Naplab.NLDatabox.Inventory.DataBox_Inventory;
using Naplab.NLUtils.Scripts.Test;
using UnityEngine;

namespace Naplab.NLDatabox.Script
{
    public class NLDataBox : MonoBehaviour
    {
        //รวมการรับค่าและส่งค่าทั้งหมดจาก databox
        
        public DataboxObjectManager ItemManager;
        public DataboxObjectManager globalManager;
        
        public const string ItemSave = "ItemSave";
        public const string ItemOrigin = "ItemOrigin";
        public const string Item = "Item";
        public const string Itemlist = "Itemlist";
        
        
        public const string Added = "Added";
        public const string Evidence = "Evidence";
        public const string Conversation = "Conversation";
        public const string Answer = "Answer";

        //Item
        public const String NPC_Name = "NPC_Name";
        public const String ItemName = "ItemName";
        public const String ItemDetail = "ItemDetail";
        public const String Relationship = "Relationship";


        public const string GlobalVariable = "GlobalVariable";
        public const string Global = "Global";
        public const string SaveLanguage = "SaveLanguage";
        public const string Language = "Language";
        public const string SaveName = "SaveName";
        public const string Name = "Name";
        public const string SaveFirstTime = "SaveFirstTime";
        public const string FirstTime = "FirstTime";
        public const string Male = "Male";
        public const string SaveMale = "SaveMale";
        public const string SaveFemale = "SaveFemale";
        public const string Female = "Female";
        public const string SaveID = "SaveID";
        public const string ID = "ID";
        public const string SaveQuiz = "SaveQuiz";
        public const string Quiz = "Quiz";
        
        public const string RoomLawyerData = "RoomLawyerData";
        public const string Tutorial = "Tutorial";
        public const string Game = "Game";
        public const string GameFirstTime = "FirstTime";
        public const string SaveGameFirstTime = "SaveGameFirstTime";
        public const string Sequence = "Sequence";
        public const string SaveSequence = "Sequence";
        public const string AccuracyPoint = "AccuracyPoint";
        public const string SaveAccuracyPoint = "SaveAccuracyPoint";
        public const string MoralityPoint = "MoralityPoint";
        public const string SaveMoralityPoint = "SaveMoralityPoint";
        public const string PolitenessPoint = "PolitenessPoint";
        public const string SavePolitenessPoint = "PolitenessPoint";
        public const string Analyze = "Analyze";
        public const string Apply = "Apply";
        public const string Evaluate = "Evaluate";
        public const string Understand = "Understand";
        public const string MissionData = "MissionData";
        
        //Part1
        public const String Part1 = "Part1";
        public const string Mission2Progress = "Progress";
        public const string Mission3Progress = "Progress";
        public const string RunTimeDataBox = "RunTimeDataBox";
        public const string GlobalDataBox = "GlobalDataBox";
        public const string SavePart1Mission1 = "SavePart1Mission1";
        public const string Part1Mission1 = "Mission1";
        public const string SavePart1Mission2 = "SavePart1Mission2";
        public const string Part1Mission2 = "Mission2";
        public const string Part1Mission7 = "Mission7";
        public const string SavePart1Mission7 = "SavePart1Mission7";

        
        //Part2
        public const string Day = "Day";
        public const string SaveDay = "SaveDay";
        public const string Energy = "Energy";
        public const string SaveEnergy = "Energy";
        public const String SavePart2 = "Part2";
        public const String Part2 = "Part2";
        public const String SlotCount = "SlotCount";
        
        public const string Mission1 = "Mission1";
        public const string SaveMission1 = "SaveMission1";
        public const string Mission2 = "Mission2";
        public const string SaveMission2 = "SaveMission2";
        public const string Mission3 = "Mission3";
        public const string SaveMission3 = "SaveMission3";
        public const string SaveMission4 = "SaveMission4";
        public const string Mission4 = "Mission4";
        public const string SaveMission5 = "SaveMission5";
        public const string Mission5 = "Mission5";
        public const string SaveMission6 = "SaveMission6";
        public const string Mission6 = "Mission6";
        public const string SaveMission7 = "SaveMission7";
        public const string Mission7 = "Mission7";
        public const string SaveMission8  = "SaveMission8";
        public const string Mission8 = "Mission8";
        public const string SaveMission9  = "SaveMission9";
        public const string Mission9 = "Mission9";
        
        //Passed
        public const string PassedMission = "PassedMission";
        public const string SavePassedMission5 = "SavePassedMission5";
        public const string SavePassedMission9 = "SavePassedMission9";
        public const string SavePassedMission7 = "SavePassedMission7";
        public const string SavePassedMission8 = "SavePassedMission8";
        
        
        //Score
        public const string Score = "Score";
        public const string Rank = "Rank";
        public const string Retry = "Retry";
        
        public const string SaveRankMS1 = "SaveRankMS1";
        public const string SaveRankMS2 = "SaveRankMS2";
        public const string SaveRankMS3 = "SaveRankMS3";
        public const string SaveRankMS4 = "SaveRankMS4";
        public const string SaveRankMS7 = "SaveRankMS7";
        
        public const string SaveRankP2MS1 = "SaveRankP2MS1";
        public const string SaveRankP2MS2 = "SaveRankP2MS2";
        public const string SaveRankP2MS3 = "SaveRankP2MS3";
        public const string SaveRankP2MS4 = "SaveRankP2MS4";
        public const string SaveRankP2MS5 = "SaveRankP2MS5";
        public const string SaveRankP2MS6 = "SaveRankP2MS6";
        public const string SaveRankP2MS7 = "SaveRankP2MS7";
        public const string SaveRankP2MS8 = "SaveRankP2MS8";
        public const string SaveRankP2MS9 = "SaveRankP2MS9";
        
        public const string SaveRetryMS1 = "SaveRetryMS1";
        public const string SaveRetryMS2 = "SaveRetryMS2";
        public const string SaveRetryMS3 = "SaveRetryMS3";
        public const string SaveRetryMS4 = "SaveRetryMS4";
        public const string SaveRetryMS7 = "SaveRetryMS7";
        
        public const string SaveRetryP2MS1 = "SaveRetryP2MS1";
        public const string SaveRetryP2MS2 = "SaveRetryP2MS2";
        public const string SaveRetryP2MS3 = "SaveRetryP2MS3";
        public const string SaveRetryP2MS4 = "SaveRetryP2MS4";
        public const string SaveRetryP2MS5 = "SaveRetryP2MS5";
        public const string SaveRetryP2MS6 = "SaveRetryP2MS6";
        public const string SaveRetryP2MS7 = "SaveRetryP2MS7";
        public const string SaveRetryP2MS8 = "SaveRetryP2MS8";
        public const string SaveRetryP2MS9 = "SaveRetryP2MS9";
        
        public const string Part2Mission1 = "Part2Mission1";
        public const string Part2Mission2 = "Part2Mission2";
        public const string Part2Mission3 = "Part2Mission3";
        public const string Part2Mission4 = "Part2Mission4";
        public const string Part2Mission5 = "Part2Mission5";
        public const string Part2Mission6 = "Part2Mission6";
        public const string Part2Mission7 = "Part2Mission7";
        public const string Part2Mission8 = "Part2Mission8";
        public const string Part2Mission9 = "Part2Mission9";
        public void SaveGame()
        {
            //Debug.Log("SaveRunTime");
            globalManager.GetDataboxObject(RunTimeDataBox).SaveDatabase();
            ItemManager.GetDataboxObject(ItemSave).SaveDatabase();
        }

        public StringType StringVariable(string DataName)
        {
            return DataName switch
            {
                SaveLanguage => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(GlobalVariable, Global, Language),
                SaveName => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(GlobalVariable, Global, Name),
                SaveID => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(GlobalVariable, Global, ID),
                
                SaveRankMS1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Mission1, Rank),
                SaveRankMS2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Mission2, Rank),
                SaveRankMS3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Mission3, Rank),
                SaveRankMS4 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Mission4, Rank),
                SaveRankMS7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Mission7, Rank),
                
                SaveRankP2MS1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission1, Rank),
                SaveRankP2MS2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission2, Rank),
                SaveRankP2MS3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission3, Rank),
                SaveRankP2MS4 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission4, Rank),
                SaveRankP2MS5 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission5, Rank),
                SaveRankP2MS6 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission6, Rank),
                SaveRankP2MS7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission7, Rank),
                SaveRankP2MS8 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission8, Rank),
                SaveRankP2MS9 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<StringType>(Score, Part2Mission9, Rank),
                _ => null
            };
        }

        public BoolType BoolVariable(string DataName)
        {
            return DataName switch
            {
                SaveFirstTime => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>(GlobalVariable, Global, FirstTime),
                SaveMale => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>(GlobalVariable, Global, Male),
                SaveFemale => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>(GlobalVariable, Global, Female),
                SaveGameFirstTime => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>(RoomLawyerData, Game, GameFirstTime),
                SaveQuiz => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>(GlobalVariable, Global, Quiz),
                SaveMission1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission1),
                SaveMission2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission2),
                SaveMission3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission3),
                SaveMission4 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission4),
                SaveMission5 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission5),
                SaveMission6 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission6),
                SaveMission7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission7),
                SaveMission8 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission8),
                SaveMission9 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part2,Mission9),
                
                SavePassedMission5 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, PassedMission,Mission5),
                SavePassedMission7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, PassedMission,Mission7),
                SavePassedMission8 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, PassedMission,Mission8),
                SavePassedMission9 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, PassedMission,Mission9),
                
                SavePart1Mission1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part1,Part1Mission1),
                SavePart1Mission2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part1,Part1Mission2),
                SavePart1Mission7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<BoolType>( MissionData, Part1,Part1Mission7),
                _ => null
            };
        }
        
        public IntType IntVariable(string DataName)
        {
            return DataName switch
            {
                SaveSequence => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(RoomLawyerData, Tutorial, Sequence),
                SaveMission2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(MissionData, Mission2, Mission2Progress),
                SaveMission3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(MissionData, Mission3, Mission3Progress),
                SaveEnergy => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(GlobalVariable ,Global, Energy),
                SaveDay => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(GlobalVariable ,Global, Day),
                
                SaveRetryMS1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Mission1, Retry),
                SaveRetryMS2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Mission2, Retry),
                SaveRetryMS3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Mission3, Retry),
                SaveRetryMS4 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Mission4, Retry),
                SaveRetryMS7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Mission7, Retry),
                
                SaveRetryP2MS1 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission1, Retry),
                SaveRetryP2MS2 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission2, Retry),
                SaveRetryP2MS3 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission3, Retry),
                SaveRetryP2MS4 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission4, Retry),
                SaveRetryP2MS5 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission5, Retry),
                SaveRetryP2MS6 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission6, Retry),
                SaveRetryP2MS7 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission7, Retry),
                SaveRetryP2MS8 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission8, Retry),
                SaveRetryP2MS9 => globalManager.GetDataboxObject(RunTimeDataBox)
                    .GetData<IntType>(Score ,Part2Mission9, Retry),

                _ => null
            };
        }
    }
}

