﻿using System;
using System.IO;
using Databox;
using Fungus;
using MalbersAnimations.Scriptables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLDatabox.Script
{
    public class DataboxManager_Exam : NLDataBox
    {
	    public TMP_InputField _inputField_Name;
	    public Toggle _ToggleFemale;
	    public Toggle _ToggleMale;
	    
	    private StringType _SaveName;
	    private BoolType _SaveFemale;
	    private BoolType _SaveMale;

	    private void Update()
	    {
		    //รับค่าจาก databox
		    _SaveName = StringVariable(SaveName);
		    _SaveFemale = BoolVariable(SaveFemale);
		    _SaveMale = BoolVariable(SaveMale);
		    //_SaveName.Value = _inputField_Name.text;
		    //_SaveFemale.Value = _ToggleFemale.isOn;
		    //_SaveMale.Value = _ToggleMale.isOn;
	    }
	    
        public void SaveData()
	    {
		    //เก็บข้อมูลลงใน databox
		    SaveGame();
	        Debug.Log("Save!");
	    }
    }
}