﻿using System;
using System.IO;
using Databox;
using Fungus;
using MalbersAnimations.Scriptables;
using Michsky.UI.ModernUIPack;
using Naplab.NLUtils.Scripts.Gameplay_Script;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Naplab.NLDatabox.Script
{
	public class DataboxManager_Gameplay : NLDataBox
	{
		public GameObject LawyerFe;
		public GameObject LawyerMa;
		public GameObject phone;
		public GameObject bookshelf;
		public GameObject Book;
		public GameObject Board;

		private StringType _SaveID;
		private StringType _SaveName;
		private StringType _SaveLanguage;
		public Flowchart _Flowchart;

		private BoolType _saveMission1;
		private BoolType _saveMission2;
		private BoolType _saveMission3;
		private BoolType _saveMission4;
		private BoolType _saveMission5;
		private BoolType _saveMission6;
		private BoolType _saveMission7;
		private BoolType _saveMission8;
		private BoolType _SaveMale;
		private BoolType _SaveFemale;
		private BoolType _SaveGameFirstTime;
		
		private BoolType _savePart1Mission1;
		private BoolType _savePart1Mission2;
		private BoolType _savePart1Mission7;
		
		private IntType _SaveSequence;
		
		public GameObject SumButt;
		public LuaUtils _Lua;
		
		//part2
		[HideInInspector] public IntType _Energy;
		public ProgressBar _EnergyBar;
		
		[HideInInspector] public IntType _Day;
		public ProgressBar _DayBar;

		private void LateUpdate()
		{
			_Energy = IntVariable(SaveEnergy);
			_SaveSequence = IntVariable(SaveSequence);
			_Day = IntVariable(SaveDay);

		}

		private void Start()
		{
			//รับข้อมูลจาก databox
			_Energy = IntVariable(SaveEnergy);
			_Day = IntVariable(SaveDay);
			_SaveFemale = BoolVariable(SaveFemale);
			_SaveMale = BoolVariable(SaveMale);
			_SaveSequence = IntVariable(SaveSequence);
			_SaveGameFirstTime = BoolVariable(SaveGameFirstTime);
			_saveMission1 = BoolVariable(SaveMission1);
			_saveMission2 = BoolVariable(SaveMission2);
			_saveMission3 = BoolVariable(SaveMission3);
			_saveMission4 = BoolVariable(SaveMission4);
			_saveMission5 = BoolVariable(SaveMission5);
			_saveMission6 = BoolVariable(SaveMission6);
			_saveMission7 = BoolVariable(SaveMission7);
			_saveMission8 = BoolVariable(SaveMission8);

			_savePart1Mission1 = BoolVariable(SavePart1Mission1);
			_savePart1Mission2= BoolVariable(SavePart1Mission2);
			_savePart1Mission7= BoolVariable(SavePart1Mission7);

			
			//เช็คว่าเลือกผู้หญิงหรือผู้ชาย
			if(_SaveFemale.Value)
			{
				LawyerFe.gameObject.SetActive(true);
				_Lua.ActiveLanguage = "THFemale";
				Flowchart.BroadcastFungusMessage("THFemale");
			}
			if(_SaveMale.Value)
			{
				LawyerMa.gameObject.SetActive(true);
				_Lua.ActiveLanguage = "THMale";
				Flowchart.BroadcastFungusMessage("THMale");
			}
			
			//เช็คว่าก่อนหน้านี้ผู้เล่นเล่นถึงไหน
			if (_SaveSequence.Value < 4&& _EnergyBar == null)
			{
				switch (_SaveSequence.Value)
				{
					case 0:
						Debug.Log("start");
						Flowchart.BroadcastFungusMessage("StartGame");
						phone.GetComponent<BoxCollider>().enabled = true;
						bookshelf.GetComponent<BoxCollider>().enabled = false;
						Book.GetComponent<BoxCollider>().enabled = false;
						Board.GetComponent<BoxCollider>().enabled = false;
						break;
					case 1:
						Flowchart.BroadcastFungusMessage("MS1Blink");
						phone.GetComponent<BoxCollider>().enabled = false;
						bookshelf.GetComponent<BoxCollider>().enabled = true;
						Book.GetComponent<BoxCollider>().enabled = false;
						Board.GetComponent<BoxCollider>().enabled = false;
						break;
					case 2:
						Flowchart.BroadcastFungusMessage("MS2Blink");
						phone.GetComponent<BoxCollider>().enabled = false;
						bookshelf.GetComponent<BoxCollider>().enabled = false;
						Book.GetComponent<BoxCollider>().enabled = true;
						Board.GetComponent<BoxCollider>().enabled = false;
						break;
					case 3:
						Flowchart.BroadcastFungusMessage("MS7Blink");
						phone.GetComponent<BoxCollider>().enabled = false;
						bookshelf.GetComponent<BoxCollider>().enabled = false;
						Book.GetComponent<BoxCollider>().enabled = false;
						Board.GetComponent<BoxCollider>().enabled = true;
						break;
				}
			}
			
			//ใน part 1 จะไม่มี energybar daybar และ flowchart
			if (_EnergyBar != null)
			{
				_EnergyBar.currentPercent = _Energy.Value;
				_DayBar.currentPercent = _Day.Value;
			}

			if (_Flowchart != null)
			{
				SyncDialog();
			}
			
		}
		
		//ฟั่งชั่นการผ่าน mission
		public void PassTutorial()
		{
			_SaveSequence.Value += 1;
			SaveGame();
			Debug.Log(_SaveSequence.Value);
			switch (_SaveSequence.Value)
			{
				case 0:
					phone.GetComponent<BoxCollider>().enabled = true;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 1:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = true;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 2:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = true;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 3:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = true;
					break;
			}
		}

		// debug ฟังชั่นให้ย้อนกลับ mission
		public void BackPart1()
		{
			_SaveSequence.Value -= 1;
			SaveGame();
			Debug.Log(_SaveSequence.Value);
			switch (_SaveSequence.Value)
			{
				case 0:
					phone.GetComponent<BoxCollider>().enabled = true;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 1:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = true;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 2:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = true;
					Board.GetComponent<BoxCollider>().enabled = false;
					break;
				case 3:
					phone.GetComponent<BoxCollider>().enabled = false;
					bookshelf.GetComponent<BoxCollider>().enabled = false;
					Book.GetComponent<BoxCollider>().enabled = false;
					Board.GetComponent<BoxCollider>().enabled = true;
					break;
			}
		}

		//debug รี energy
		public void ResetEnergy()
		{
			_Energy.Value = 15;
			_EnergyBar.currentPercent = _Energy.Value;
		}

		//debug รี day
		public void ResetDay()
		{
			_Day.Value = 1;
			_DayBar.currentPercent = _Day.Value;
		}

		//debug ลดและเพิ่ม energy
		public void NegativeEnergy()
		{
			if (_EnergyBar.currentPercent > 0)
			{
				_Energy.Value -= 1;
				SaveGame();
				_EnergyBar.currentPercent = _Energy.Value;
			}
			else
			{
				Flowchart.BroadcastFungusMessage("NotEnoughEnergy");
			}
		}
		public void PlusEnergy()
		{
			if (_EnergyBar.currentPercent > 0)
			{
				_Energy.Value += 1;
				SaveGame();
				_EnergyBar.currentPercent = _Energy.Value;
			}
			else
			{
				Flowchart.BroadcastFungusMessage("NotEnoughEnergy");
			}
		}

		//debug ได้ item ท้งหมดใน part 2
		public void GetAllItem()
		{
			var jsonString = Resources.Load<TextAsset>("Item");
			File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, ItemSave), jsonString.text);
			ItemManager.GetDataboxObject(ItemSave).LoadDatabase();
		}

		//ฟังชั่นพักผ่อน
		public void Rest()
		{
			_Day.Value += 1;
			SaveGame();
			_DayBar.currentPercent = _Day.Value;
		}

		//ฟังชั่นตรวจสอบว่าคุยกับใครไปแล้วบ้างใน part 2
		public void SyncDialog()
		{
			//ส่งค่าจาก databox ไป flowchart ว่าคุยบทสนทนาไหนไปแล้วบ้าง
			foreach (var Value in globalManager.GetDataboxObject(RunTimeDataBox).DB["DialogP2"].entries["Dialog"].data)
			{
				var name = globalManager.GetDataboxObject(RunTimeDataBox)
					.GetData<BoolType>("DialogP2", "Dialog", Value.Key);
				
				_Flowchart.SetBooleanVariable(Value.Key,name.Value);
			}
		}
		public void SetDataboxDialog()
		{
			//รับค่าจาก flowchart ไป databox
			foreach (var Value in globalManager.GetDataboxObject(RunTimeDataBox).DB["DialogP2"].entries["Dialog"].data)
			{
				var name = globalManager.GetDataboxObject(RunTimeDataBox)
					.GetData<BoolType>("DialogP2", "Dialog", Value.Key);
				
				name.Value = _Flowchart.GetBooleanVariable(Value.Key);
			}
			SaveGame();
		}
		
	}
}