﻿using System;
using System.IO;
using Databox;
using Fungus;
using MalbersAnimations.Scriptables;
using Naplab.NLDatabox.Inventory.Script;
using Naplab.NLUtils.Scripts.Test;
using Sirenix.Utilities;
using UnityEngine;

namespace Naplab.NLDatabox.Script
{
    public class DataboxManager_Global : NLDataBox
    {
	    private DataboxObject _RuntimeData;
	    private StringType _SaveLanguage;
	    private BoolType _SaveFirstTime;
	    private BoolType _SaveMale;
	    private BoolType _SaveFemale;
	    private BoolType _SaveQuiz;
	    public DataboxManager_Gameplay _Gameplay;
	    
	    [SerializeField] private LuaUtils Lua_Utils;

	    private void Start()
	    {
		    //เช็คว่ามีข้อมูล save รึยัง
		    //ถ้าไม่มีให้สร้างมาใหม่
		    if (File.Exists(Application.persistentDataPath + "/GlobalData"))
		    {
			    _RuntimeData.LoadDatabase();
			    Debug.Log("LoadSave");
		    }
		    else
		    {
			    var jsonString = Resources.Load<TextAsset>("GlobalData");
			    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "GlobalData"), jsonString.text);
			    _RuntimeData.LoadDatabase();
			    Debug.Log("NewSave");
		    }
		    
		    if (File.Exists(Application.persistentDataPath + "/ItemSave")&&File.Exists(Application.persistentDataPath + "/ItemOrigin")&& _Gameplay != null)
		    {
			    ItemManager.GetDataboxObject("ItemOrigin").LoadDatabase();
			    ItemManager.GetDataboxObject(ItemSave).LoadDatabase();
			    Debug.Log("LoadItem");
		    }
		    else
		    {
			    var jsonString = Resources.Load<TextAsset>("Item");
			    var EmptyString = Resources.Load<TextAsset>("ItemEmpty");
			    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "ItemOrigin"), jsonString.text);
			    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "ItemSave"), EmptyString.text);
			    ItemManager.GetDataboxObject("ItemOrigin").LoadDatabase();
			    ItemManager.GetDataboxObject(ItemSave).LoadDatabase();
			    Debug.Log("NewItem");
		    }
		    
	    }

	    //รีเกมทั้งหมด
	    public void ResetGame()
	    {
		    var AllItem = Resources.FindObjectsOfTypeAll<Call_Info>();
		    foreach (var Item in AllItem)
		    {
			    Item._ItemState = ItemState.Instance;
		    }
		    
		    var jsonString = Resources.Load<TextAsset>("GlobalData");
		    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "GlobalData"), jsonString.text);
		    _RuntimeData.LoadDatabase();
		    
		    var ItemString = Resources.Load<TextAsset>("Item");
		    var EmptyString = Resources.Load<TextAsset>("ItemEmpty");
		    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "ItemOrigin"), ItemString.text);
		    File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "ItemSave"), EmptyString.text);
		    ItemManager.GetDataboxObject("ItemOrigin").LoadDatabase();
		    ItemManager.GetDataboxObject(ItemSave).LoadDatabase();
		    if (_Gameplay != null)
		    {
			    _Gameplay.SyncDialog();
			    _Gameplay.SetDataboxDialog();
			    _Gameplay.ResetEnergy();
			    _Gameplay.ResetDay();
		    }
		    Debug.Log("Resetto");
	    }
	    
	    private void OnEnable()
        {
	        _RuntimeData = globalManager.GetDataboxObject(RunTimeDataBox);
	        _RuntimeData.OnDatabaseLoaded += DataReady;
        }
        private void OnDisable()
        {
	        _RuntimeData.OnDatabaseLoaded -= DataReady;
        }
        void DataReady()
	    {
		    //รับค่าจาก databox
		    _SaveLanguage = StringVariable(SaveLanguage);
		    _SaveFirstTime = BoolVariable(SaveFirstTime);
		    _SaveMale = BoolVariable(SaveMale);
		    _SaveFemale = BoolVariable(SaveMale);
		    _SaveQuiz = BoolVariable(SaveQuiz);

	    }
        public void ConfirmFirstTime()
        {
	        _SaveFirstTime.Value = true;
	        SaveGame();
        }

        public void PassQuiz()
        {
	        _SaveQuiz.Value = true;
	        SaveGame();
        }
        #region ChangeLanguage
        public void SetLanguage()
	    {
		    // if(Lua_Utils.ActiveLanguage == "THFemale"||Lua_Utils.ActiveLanguage == "THMale")
	     //    {
		    //     Lua_Utils.ActiveLanguage = "EN";
		    //     _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
		    //     Flowchart.BroadcastFungusMessage("ChangeLanMenu");
	     //    }
	     //    else if(Lua_Utils.ActiveLanguage == "EN")
	     //    {
		    //     if(_SaveFemale.Value)
		    //     {
			   //      Lua_Utils.ActiveLanguage = "THFemale";
			   //      _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
		    //     }
		    //     switch (_SaveMale.Value)
		    //     {
			   //      case true:
				  //       Lua_Utils.ActiveLanguage = "THMale";
				  //       _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
				  //       break;
			   //      case false when _SaveMale.Value == false:
				  //       Lua_Utils.ActiveLanguage = "THMale";
				  //       _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
				  //       break;
		    //     }
		    //     
		    //     _SaveLanguage.Value = _SaveLanguage.Value;
		    //     Flowchart.BroadcastFungusMessage("ChangeLanMenu");
	     //    }
	    }

        //Use in MenuScene At FlowchartMenu
        public void SelectTH()
        {
	        Lua_Utils.ActiveLanguage = "THFemale";
	        _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
	        _SaveLanguage.Value = _SaveLanguage.Value;
        }
        
        // public void SelectEN()
        // {
	       //  Lua_Utils.ActiveLanguage = "EN";
	       //  _SaveLanguage.Value = Lua_Utils.ActiveLanguage;
	       //  _SaveLanguage.Value = _SaveLanguage.Value;
        // }
        #endregion
    }
}