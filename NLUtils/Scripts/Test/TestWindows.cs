﻿using System;
using MalbersAnimations.Scriptables;
using Naplab.NLDatabox.Script;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Test
{
	public class TestWindows : NLDataBox
	{
		[SerializeField] private Text SequenceText;
		[SerializeField] private Text AcText;
		[SerializeField] private Text MoText;
		[SerializeField] private Text PoText;
		[SerializeField] private Text LanText;
		[SerializeField] private Text NameText;
		[SerializeField] private Text SetNameText;
		private StringType _SaveLanguage;
		private StringType _SaveName;
		private IntType _SaveSequence;
		private BoolType _SaveFirstTime;
		private BoolType _SaveMale;
		private BoolType _SaveFemale;

		private void Start()
		{
			_SaveLanguage = StringVariable(SaveLanguage);
			_SaveName = StringVariable(SaveName);
			_SaveSequence = IntVariable(SaveSequence);
		}

		void Update()
		{
		
			//SetName To Text
			SetNameText.text = _SaveName.Value.ToString();
		
			SequenceText.text = "Current Mission = " + _SaveSequence.Value.ToString();
			// AcText.text = "Accuracy Point = " + AcPoint.Value.ToString();
			// MoText.text = "Morality Point = " + MoPoint.Value.ToString();
			// PoText.text = "Politeness Point = " + PoPoint.Value.ToString();
			LanText.text = "Current Language = " + _SaveLanguage.Value.ToString();
			NameText.text = "Player Name = " + _SaveName.Value.ToString();
		}
	}
}
