using UnityEngine;

namespace Naplab.NLUtils.Scripts.Pause
{
    public class PauseGameLawyer : MonoBehaviour
    {
        // Start is called before the first frame update
        public void PauseGame ()
        {
            Time.timeScale = 0;
        }

        public void ResumeGame ()
        {
            Time.timeScale = 1;
        }
    }
}
