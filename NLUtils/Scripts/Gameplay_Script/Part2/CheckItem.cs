using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Inventory.DataBox_Inventory;
using UnityEngine;

public class CheckItem : MonoBehaviour
{
    public GameObject _PanelC;
    public GameObject _PanelE;

    public void CheckitemPanel()
    {
        if (_PanelC.transform.childCount <= 0)
        {
            _PanelC.GetComponent<ItemLoad>().loadConversation();
        }
        if (_PanelE.transform.childCount <= 0)
        {
            _PanelE.GetComponent<ItemLoad>().loadEvidence();
        }
    }
}
