using System.Collections;
using System.Collections.Generic;
using MalbersAnimations.Events;
using UnityEngine;

public class ResetPos : MonoBehaviour
{
    public GameObject LawyerMa;
    public GameObject LawyerFe;
    public GameObject ResetSpot;
    
    public void ResetPosition()
    {
        var position = ResetSpot.transform.position;
        LawyerFe.transform.position = position;
        LawyerMa.transform.position = position;
    }
}
