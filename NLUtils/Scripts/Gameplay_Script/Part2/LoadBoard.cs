using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadBoard : MonoBehaviour
{
    public void ActiveBoard()
    {
        gameObject.GetComponent<Canvas>().planeDistance = 0.5f;
    }

    public void DisableBoard()
    {
        gameObject.GetComponent<Canvas>().planeDistance = -1f;
    }
}
