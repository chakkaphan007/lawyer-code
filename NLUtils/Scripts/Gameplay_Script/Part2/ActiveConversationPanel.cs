using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveConversationPanel : MonoBehaviour
{
    public void Activate()
    {
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-317, -58.082f, 0);
    }

    public void Disable()
    {
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-317, -58.082f, -2000);
    }
}
