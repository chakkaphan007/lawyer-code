﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Mission_4
{
	public class PairWord : MonoBehaviour
	{
		private int _choice;
		[SerializeField] private int _dialog;
		
		//รับคำตอบมา
		public void GetPair(int Choice)
		{
			_choice = Choice;
		}

		public int CheckChoice()
		{
			//ตรวจสอบว่าคำตอบที่ได้มาตรงกับที่กำหนดไว้ไหม
			this.gameObject.GetComponent<Toggle>().isOn = true;
			switch (_choice)
			{
				case 3 when _dialog == 1:
					return 1;
				case 2 when _dialog == 2:
					return 2;
				case 6 when _dialog == 3:
					return 3;
				case 5 when _dialog == 4:
					return 4;
			}
			if ((_choice != 3 && _dialog == 1)) return -1;
			if ((_choice != 2 && _dialog == 2)) return -2;
			if ((_choice != 6 && _dialog == 3)) return -3;
			if ((_choice != 5 && _dialog == 4)) return -4;
			return 0;
		}
		public void CheckDrop()
		{
			//ถ้าคำตอบที่ได้รับมาตรงกับที่กำหนดไว้ให้ส่งค่ากลับว่าถูกต้อง
			CheckChoice();
			if (CheckChoice() == 1)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_1 = true;
			}
			if (CheckChoice() == 2)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_2 = true;
			}
			if (CheckChoice() == 3)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_3 = true;
			}
			if (CheckChoice() == 4)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_4 = true;
			}
			if (CheckChoice() == -1)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_1 = false;
			}
			if (CheckChoice() == -2)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_2 = false;
			}
			if (CheckChoice() == -3)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_3 = false;
			}
			if (CheckChoice() == -4)
			{
				CheckPair.FindObjectOfType<CheckPair>()._dialog_4 = false;
			}
			Debug.Log(CheckChoice());
		}
		
	}
}
