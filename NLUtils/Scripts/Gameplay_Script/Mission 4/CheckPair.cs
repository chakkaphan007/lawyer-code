using System;
using System.Collections;
using System.Collections.Generic;
using Fungus;
using Naplab.NLUtils.Scripts.Score_Scrpt;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Mission_4
{
    public class CheckPair : MonoBehaviour
    {
        public PartOneScore _partOneScore;
        public bool _dialog_1;
        public bool _dialog_2;
        public bool _dialog_3;
        public bool _dialog_4;
        public List<Toggle> ToggleDialog;
        [SerializeField] private Button Confirm;
        public void Checking()
        {
            //ถ้า bool = true ทั้งหมด ให้ผ่าน
            if (_dialog_1 && _dialog_2 && _dialog_3 && _dialog_4)
            {
                Flowchart.BroadcastFungusMessage("MS4Correct");
                Debug.Log("pass");
                _partOneScore.Mission4Pass();
            }
            else
            {
                if (_dialog_1 == false)
                {
                    ToggleDialog[0].isOn = false;
                }
                if (_dialog_2 == false)
                {
                    ToggleDialog[1].isOn = false;
                }
                if (_dialog_3 == false)
                {
                    ToggleDialog[2].isOn = false;
                }
                if (_dialog_4 == false)
                {
                    ToggleDialog[3].isOn = false;
                }
                _partOneScore.Mission4Fail();
            }
        }

        private void Update()
        {
            //เช็คว่า คำตอบที่นำมาใส่ถูกหรือไม่
            Confirm.interactable =
                ToggleDialog[0].isOn
                && ToggleDialog[1].isOn 
                && ToggleDialog[2].isOn 
                && ToggleDialog[3].isOn;
        }
        
    }
}
