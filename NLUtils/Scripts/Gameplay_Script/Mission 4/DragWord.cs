﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Fungus;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Mission_4
{
public class DragWord : MonoBehaviour ,IPointerClickHandler,IBeginDragHandler,IEndDragHandler,IDragHandler,IPointerUpHandler
{
	[SerializeField] private Canvas canvas;
	[SerializeField] private RectTransform Rectransform;
	[SerializeField] private Transform ReRect;
	[SerializeField] private CanvasGroup canvasgroup;
	
	private string State;
	public int Choice;

	private void Awake()
	{
		Rectransform = GetComponent<RectTransform>();	
		canvasgroup = GetComponent<CanvasGroup>();	
	}
	
	public void OnBeginDrag(PointerEventData EventData)
	{
		//ให้ maskable = false เพราะรูปจะไม่ถูกบัง
		gameObject.GetComponent<Image>().maskable = false;
		gameObject.GetComponentInChildren<TextMeshProUGUI>().maskable = false;
		//ปิดเพื่อให้สามารถ drag ได้
		canvasgroup.blocksRaycasts = false;
	}
	public void OnDrag(PointerEventData EventData)
	{
		//ให้ตำแหน่งของ object ติดไปตาม mouse
		Rectransform.anchoredPosition += EventData.delta / canvas.scaleFactor;
	}
	public void OnEndDrag(PointerEventData EventData)
	{
		//เปิด Maskable 
		gameObject.GetComponent<Image>().maskable = true;
		gameObject.GetComponentInChildren<TextMeshProUGUI>().maskable = true;
		
		//ให้ตรวจสอบว่าตอนนี้ mouse ทับอยู่บน object อะไร
		List<GameObject> detectList = EventData.hovered;
		foreach (var GO in detectList)
		{
			//ถ้าทับตรงกับ GO.name ตามที่เรากำหนดให้เข้าฟังชั่น
			Debug.Log("Detecting: " + GO.name);
			if (GO.name == "Dialog_1"&& GO.transform.childCount == 0)
			{
					//เก็บค่า state ไว้เป็นชื่อของ object เพื่อนำไปเช็คเงื่อนไขใน ResetPos
					State = GO.name;
					//หา GameObject
					var dialog1 = GameObject.Find(State).transform;
					//ให้ Object นี้ย้ายไปเป็น child เพื่อให้เมื่อ Drag รูปจะเลื่อนตามได้ถูกต้อง
					transform.SetParent(dialog1);
					//ให้ object นี้อยู่ตรง 0,0
					Rectransform.anchoredPosition = Vector2.zero;
					//ส่งค่า Choice ไปที่ PairWord เพื่อให้รู้ว่าใส่ข้ออะไรไป
					GO.GetComponent<PairWord>().GetPair(Choice);
					//สั่งให้เช็คคำตอบ
					GO.GetComponent<PairWord>().CheckDrop();
					Debug.Log(State);
					break;
			}

			if (GO.name == "Dialog_2"&& GO.transform.childCount == 0)
			{
				State = GO.name;
				var dialog1 = GameObject.Find(State).transform;
				transform.SetParent(dialog1);
				Rectransform.anchoredPosition = Vector2.zero;
				GO.GetComponent<PairWord>().GetPair(Choice);
				GO.GetComponent<PairWord>().CheckDrop();
				Debug.Log(State);
				break;
			}

			if (GO.name == "Dialog_3"&& GO.transform.childCount == 0)
			{
				State = GO.name;
				var dialog1 = GameObject.Find(State).transform;
				transform.SetParent(dialog1);
				Rectransform.anchoredPosition = Vector2.zero;
				GO.GetComponent<PairWord>().GetPair(Choice);
				GO.GetComponent<PairWord>().CheckDrop();
				Debug.Log(State);
				break;
			}

			if (GO.name == "Dialog_4"&& GO.transform.childCount == 0)
			{
				State = GO.name;
				var dialog1 = GameObject.Find(State).transform;
				transform.SetParent(dialog1);
				Rectransform.anchoredPosition = Vector2.zero;
				GO.GetComponent<PairWord>().GetPair(Choice);
				GO.GetComponent<PairWord>().CheckDrop();
				Debug.Log(State);
				break;
			}
			
			//ถ้าไม่ตรงเงื่อนไขใดๆให้ Reset กลับ
			ResetPos();
			canvasgroup.blocksRaycasts = true;
		}
		canvasgroup.blocksRaycasts = true;
	}
	public void ResetPos()
	{
		//เช็ค State เพื่อให้รู้ว่านำคำตอบออกจากข้อไหน
		if (State == "Dialog_1")
		{
			//ปิด toggle เพื่อให้สามารถนำคำตอบมาใส่ได้
			FindObjectOfType<CheckPair>().ToggleDialog[0].isOn = false;
			//ย้าย object ไปที่ ReRect
			transform.SetParent(ReRect);
			Rectransform.anchoredPosition = Vector2.zero;
			State = null;
		}
		else if (State == "Dialog_2")
		{
			FindObjectOfType<CheckPair>().ToggleDialog[1].isOn = false;
			transform.SetParent(ReRect);
			Rectransform.anchoredPosition = Vector2.zero;
			State = null;
		}
		else if (State == "Dialog_3")
		{
			FindObjectOfType<CheckPair>().ToggleDialog[2].isOn = false;
			transform.SetParent(ReRect);
			Rectransform.anchoredPosition = Vector2.zero;
			State = null;
		}
		else if (State == "Dialog_4")
		{
			FindObjectOfType<CheckPair>().ToggleDialog[3].isOn = false;
			transform.SetParent(ReRect);
			Rectransform.anchoredPosition = Vector2.zero;
			State = null;
		}
		else
		{
			transform.SetParent(ReRect);
			Rectransform.anchoredPosition = Vector2.zero;
			State = null;
			Debug.Log("Reset");
		}
	}
	
	public void CheckingVaild()
	{
		//เช็คว่าคำตอบที่ใส่ถูกหรือไม่ ถ้าไม่ถูกต้องให้ Reset
		if (State == "Dialog_1"&&!FindObjectOfType<CheckPair>().ToggleDialog[0].isOn)
		{
			ResetPos();
		}
		if (State == "Dialog_2"&&!FindObjectOfType<CheckPair>().ToggleDialog[1].isOn)
		{
			ResetPos();
		}
		if (State == "Dialog_3"&&!FindObjectOfType<CheckPair>().ToggleDialog[2].isOn)
		{
			ResetPos();
		}
		if (State == "Dialog_4"&&!FindObjectOfType<CheckPair>().ToggleDialog[3].isOn)
		{
			ResetPos();
		}
	}
	public void OnPointerClick(PointerEventData eventData)
	{
		canvasgroup.blocksRaycasts = true;
	}
	public void OnPointerUp(PointerEventData eventData)
	{
		canvasgroup.blocksRaycasts = true;
	}
}
}
