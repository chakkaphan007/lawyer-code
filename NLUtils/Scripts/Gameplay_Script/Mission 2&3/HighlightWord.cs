using System.Collections.Generic;
using Fungus;
using Naplab.NLDatabox.Script;
using Naplab.NLUtils.Scripts.Score_Scrpt;
using Naplab.NLUtils.Scripts.UI_Script;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Mission_2_3
{
	public class HighlightWord : NLDataBox
	{
		private int HighlightCount = 0;

		public PartOneScore _PartOneScore;
		
		public Mission2ProgressBar _ProgressBar1;
		public Mission2ProgressBar _ProgressBar2;
		
		public GameObject Panel_Mission2;
		public GameObject Panel_Mission3;

		public Button ConfirmButt2;
		public Button ConfirmButt3;
		public Button Mark1;
		public Button Mark2;
		public Button Mark3;
		
		public Toggle Pen1;
		public Toggle Eraser1;
		
		public Toggle Pen2;
		public Toggle Eraser2;

		public List<Toggle> Mission2Toggle;
		public List<Toggle> Mission3Toggle;
		

		private void Update()
		{
			//ตั้งให้ Progressbar เท่ากับจำนวนที่ highlight
			_ProgressBar1.currentPercent = HighlightCount;
			_ProgressBar2.currentPercent = HighlightCount;
			
			//ให้ปุ่ม Confirm ทำงานได้เมื่อ highlightcount มากกว่า 4
			ConfirmButt3.interactable = HighlightCount >= 4;
		}

		public void CheckHighlightMS2(int Number)
		{
			//เช็คว่า hightlight ตรงไหน
			if (HighlightCount < 4 && Pen1.isOn && !Mission2Toggle[Number].isOn)
			{
				Mission2Toggle[Number].isOn = true;
				HighlightCount += 1;
				SaveGame();
			}
			//เช็คว่าลบ highlight ตรงไหน
			if (Eraser1.isOn && Mission2Toggle[Number].isOn)
			{
				Mission2Toggle[Number].isOn = false;
				HighlightCount -= 1;
				SaveGame();
			}
		}
		
		public void CheckHighlightMS3(int Number)
		{
			//เช็คว่า hightlight ตรงไหน
			if (HighlightCount < 4 && Pen2.isOn && !Mission3Toggle[Number].isOn)
			{
				Mission3Toggle[Number].isOn = true;
				HighlightCount += 1;
				SaveGame();
			}
			//เช็คว่าลบ highlight ตรงไหน
			if (Eraser2.isOn && Mission3Toggle[Number].isOn)
			{
				Mission3Toggle[Number].isOn = false;
				HighlightCount -= 1;
				SaveGame();
			}
		}

		public void ConfirmMission2()
		{
			int amoung = 0;
			//เช็คว่า hightlight ตรงกับคำตอบที่กำหนดไว้ไหม
			// ถ้าใช่ให้เพิ่มค่า amoung และให้ toggle = on ถ้าไม่ใช่ให้ปิด toggle
			foreach (var t in Mission2Toggle)
			{
				switch (t.isOn)
				{
					case true when t == Mission2Toggle[1]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission2Toggle[2]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission2Toggle[8]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission2Toggle[9]:
						amoung++;
						t.isOn = true;
						break;
					case true:	
						Flowchart.BroadcastFungusMessage("MS2Retry");
						t.isOn = false;
						break;
				}
				HighlightCount = amoung;
			}
			
			//ถ้า highlight ตรงกับที่กำหนดไว้ทั้งหมด ให้ผ่านได้
			if (Mission2Toggle[1].isOn&& Mission2Toggle[2].isOn&& Mission2Toggle[8].isOn&& Mission2Toggle[9].isOn)
			{
				Flowchart.BroadcastFungusMessage("MS2Correct");
				HighlightCount = 0;
				Mark1.interactable = false;
				Mark2.interactable = true;
				_PartOneScore.Mission2Pass();
			}
			else
			{
				_PartOneScore.Mission2Fail();
			}
			
		}
		public void ConfirmMission3()
		{
			int amoung = 0;
			//เช็คว่า hightlight ตรงกับคำตอบที่กำหนดไว้ไหม
			// ถ้าใช่ให้เพิ่มค่า amoung และให้ toggle = on ถ้าไม่ใช่ให้ปิด toggle
			foreach (var t in Mission3Toggle)
			{
				switch (t.isOn)
				{
					case true when t == Mission3Toggle[0]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission3Toggle[6]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission3Toggle[7]:
						amoung++;
						t.isOn = true;
						break;
					case true when t == Mission3Toggle[8]:
						amoung++;
						t.isOn = true;
						break;
					case true:
						Flowchart.BroadcastFungusMessage("MS2Retry");
						t.isOn = false;
						break;
				}
				HighlightCount = amoung;
			}
			//ถ้า highlight ตรงกับที่กำหนดไว้ทั้งหมด ให้ผ่านได้
			if (Mission3Toggle[0].isOn&& Mission3Toggle[6].isOn&& Mission3Toggle[7].isOn&& Mission3Toggle[8].isOn)
			{
				Flowchart.BroadcastFungusMessage("MS3Correct");
				Mark2.interactable = false;
				Mark3.interactable = true;
				_PartOneScore.Mission3Pass();
			}
			else
			{
				_PartOneScore.Mission3Fail();
			}
		}
	}
}
