﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Mission_2_3
{
	public class ItemSlot : MonoBehaviour
	{
		public int Number;
		private HighlightWord _highlight;

		private void Start()
		{
			_highlight = FindObjectOfType<HighlightWord>();
		}

		public void selectMS2()
		{
			//ส่งค่ากลับไปที่  CheckHighlightMS2
			_highlight.CheckHighlightMS2(Number);
		}

		public void SelectMS3()
		{
			//ส่งค่ากลับไปที่  CheckHighlightMS3
			_highlight.CheckHighlightMS3(Number);
		}
	}
}
