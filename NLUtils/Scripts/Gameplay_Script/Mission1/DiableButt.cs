using System.Collections;
using System.Collections.Generic;
using Fungus;
using Naplab.NLUtils.Scripts.Test;
using UnityEngine;
using UnityEngine.UI;

public class DiableButt : MonoBehaviour
{
    public GameObject C1;
    public GameObject C2;
    public GameObject C3;
    public GameObject C4;
    public GameObject C5;

    //ปิด GameObject ไม่ให้กดได้
    public void DisableC1()
    {
        C1.GetComponent<Button>().interactable = false;
    }
    public void DisableC2()
    {
        C2.GetComponent<Button>().interactable = false;
    }
    public void DisableC3()
    {
        C3.GetComponent<Button>().interactable = false;
    }
    public void DisableC4()
    {
        C4.GetComponent<Button>().interactable = false;
    }
    public void DisableC5()
    {
        C5.GetComponent<Button>().interactable = false;
    }

    public Flowchart _Flowchart;
    
    public void EnableC()
    {
        _Flowchart.SetBooleanVariable("C1",false);
        _Flowchart.SetBooleanVariable("C2",false);
        _Flowchart.SetBooleanVariable("C3",false);
        _Flowchart.SetBooleanVariable("C4",false);
        _Flowchart.SetBooleanVariable("C5",false);
    }
}
