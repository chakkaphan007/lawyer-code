using System.Collections;
using System.Collections.Generic;
using Fungus;
using UnityEngine;

public class Return : MonoBehaviour
{
    public string Choice;
    public Flowchart _Flowchart;

    public void SetChoice()
    {
        //ส่งค่า choice กลับไปที่ flowchart
        _Flowchart.SetBooleanVariable(Choice,true);
    }
}
