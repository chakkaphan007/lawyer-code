using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerSwapping : MonoBehaviour
{
    public GameObject[] Answer;
    int rand;
    public void AnswerSwap()
    {
        //random ปุ่ม
        for (int i = 0; i < Answer.Length; i++)
        {
            rand = Random.Range(0, 19);
            Answer[i].transform.SetSiblingIndex(rand);
        }
    }
}
