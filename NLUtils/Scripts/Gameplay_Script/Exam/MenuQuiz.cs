using System;
using System.Collections.Generic;
using Fungus;
using UnityEngine;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Gameplay_Script.Exam
{
    public class MenuQuiz : MonoBehaviour
    {
        [SerializeField] private Flowchart _flowchart;
        public int _QuizNumber;
        public int AnswerIndex;
        public int Answer;
        public Exam_Progressbar _ExamProgressbar;
        public Button _ConfirmButt;
        
        public List<Button> AnswerButt;


        private void Start()
        {
            ActiveMenu();
        }

        private void Update()
        {
            //รับค่าของคำถามว่าอย่ที่คำถามที่เท่าไหร่
            _QuizNumber = _flowchart.GetIntegerVariable("QuizNumber");
            //รับค่าจำนวนของคำตอบ
            AnswerIndex = _flowchart.GetIntegerVariable("AnsIndex");
            
            _ExamProgressbar.currentPercent = _QuizNumber;
            _ConfirmButt.interactable = Answer != 0;
        }
        
        public void InteractButt()
        {
            //เปิดคำตอบใน list ตามที่ได้รับจาก Flowchart
            for (int i = 0; i < AnswerIndex; i++)
            {
                AnswerButt[i].interactable = true;
            }
            //ปิดคำตอบที่ถูกเลือก
            AnswerButt[Answer - 1].interactable = false;
        }
        public void Confirm()
        {
            //ส่ง answer ไปที่ flowchart
            _flowchart.SetIntegerVariable("ReturnAns", Answer);
            //สั่งให้  check ทำงาน
            Flowchart.BroadcastFungusMessage("Check"); 
            Answer = 0;
            ClearMenu();
        }
        
        public void ActiveMenu()
        {
            //เปิดปุ่มคำตอบ
            for (int i = 0; i < AnswerIndex; i++)
            {
                AnswerButt[i].gameObject.SetActive(true);
            }
            //ตั้งให้ปุ่ม interactable
            for (int i = 0; i < AnswerButt.Count; i++)
            {
                AnswerButt[i].interactable = true;
            }
        }

        public void ClearMenu()
        {
            //ปิดปุ่มคำตอบ
            for (int i = 0; i < AnswerButt.Count; i++)
            {
                AnswerButt[i].gameObject.SetActive(false);
            }
        }
    }
}
