using System.Collections;
using System.Collections.Generic;
using Naplab.NLUtils.Scripts.Gameplay_Script.Exam;
using UnityEngine;
 
public class AnswerReturn : MonoBehaviour
{
    public int Return;

    public void ReturnAns()
    {
        //ส่งค่า Return กลับไปที่ MenuQuiz
        FindObjectOfType<MenuQuiz>().Answer = Return;
    }
}
