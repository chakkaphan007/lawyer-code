﻿using System.Collections.Generic;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Gameplay_Script
{
	public class ChangeMaterial : MonoBehaviour
{
    Material Material;
    MeshRenderer meshRenderer;
    public List<Material> materials;
    public int index;
    // Use this for initialization
    void Start()
    {
        index = 0;
        meshRenderer = GetComponent<MeshRenderer>();
    }
    void Update()
    {
        //เปลี่ยน material
        Material = materials[index];
        meshRenderer.material = Material;
    }
    public void blink()
    {
        index = 1;
    }
        public void Normal()
    {
        index = 0;
    }
}
}
