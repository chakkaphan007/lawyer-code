using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Menu_Script
{
 public class SceneManager : MonoBehaviour
 {
     private Stack<int> loadedLevels;

     [System.NonSerialized]
     private bool initialized;

     public static object GetSceneAt;

     public static int sceneCount { get; }
     
     private void Init()
     {
         loadedLevels = new Stack<int>();
         initialized = true;
     }
  
     public UnityEngine.SceneManagement.Scene GetActiveScene()
     {
         //รับ Scene มา
         return UnityEngine.SceneManagement.SceneManager.GetActiveScene();
     }
     
     public void LoadScene( int buildIndex)
     {
         if ( !initialized ) Init();
         StartCoroutine(this.WaitTime(buildIndex));
     }
 
     public void LoadScene( string sceneName )
     {
         if ( !initialized ) Init();
         StartCoroutine(this.WaitTime2(sceneName));

     }
     
     public void LoadPreviousScene()
     {
         if ( loadedLevels.Count > 0 )
         {
             UnityEngine.SceneManagement.SceneManager.LoadScene( loadedLevels.Pop() );
         }
     }

     public IEnumerator WaitTime(int buildIndex)
     {
         //รอ 0.5 ตามระยะเวลา Fade 
         yield return new WaitForSeconds(0.5f);
         //โหลด Scene ใน buildindex
         loadedLevels.Push( GetActiveScene().buildIndex );
         UnityEngine.SceneManagement.SceneManager.LoadScene( buildIndex );
     }
     
     public IEnumerator WaitTime2(string sceneName)
     {
         yield return new WaitForSeconds(0.5f);
         loadedLevels.Push( GetActiveScene().buildIndex );
         UnityEngine.SceneManagement.SceneManager.LoadScene( sceneName );
     }
 }
}
