using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Naplab.NLUtils.Scripts.Menu_Script
{
public class VolumeControl_Script : MonoBehaviour
{

    public Slider slider;
    public AudioMixer mixer;
    string Volume = "MasterMusic";
    void Awake()
    {
        slider.onValueChanged.AddListener(HandleSliderValueChanged);
    }

    void OnDisable()
    {
        //เก็บ Voluem floar
        PlayerPrefs.SetFloat(Volume,slider.value);
    }
    void HandleSliderValueChanged(float Value)
    {
        //ให้ value ใน slider เท่ากับ mixer
        mixer.SetFloat(Volume,Mathf.Log10(Value) * 30f);
    }

    void Start()
    {
        //ให้ sllider เท่ากับค่า float ที่เก็บไว้
        slider.value = PlayerPrefs.GetFloat(Volume,slider.value);
    }
}
}
