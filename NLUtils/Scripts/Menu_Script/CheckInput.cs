using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CheckInput : MonoBehaviour
{
    public Toggle Male;
    public Toggle Female;
    public InputField Name;
    public Button Next;

    void Update()
    {
        //ตรวจสอบว่า input มี sting อยู่หรือไม่
        Next.interactable = (Male.isOn || Female.isOn) && !String.IsNullOrEmpty(Name.text);
    }
}
