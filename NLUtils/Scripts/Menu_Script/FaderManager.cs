using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Menu_Script
{
    public class FaderManager : MonoBehaviour
    {
    [SerializeField] private List<RectTransform> fader;
    private float _toWait;
    // Start is called before the first frame update
    void Start()
    {
        //ระยะเวลาที่รอ
        _toWait = 0.5f;
        //ให้ object แสดง
        fader[0].gameObject.SetActive(true);
        //ให้ object เปลี่ยน Scale เป็น 1,1,1
        LeanTween.scale(fader[0], new Vector3(1,1, 1), 0);
        LeanTween.scale(fader[0],Vector3.zero, _toWait).setEase(LeanTweenType.clamp).setOnComplete((() =>
        {
            fader[0].gameObject.SetActive(false);
        }));
    }

    public void StartSelect()
    {
        _toWait = 0.5f;
        fader[1].gameObject.SetActive(true);
        LeanTween.scale(fader[1], new Vector3(1,1, 1), 0);
        LeanTween.scale(fader[1],Vector3.zero, _toWait).setEase(LeanTweenType.clamp).setOnComplete((() =>
        {
            fader[1].gameObject.SetActive(false);
        }));
    }
    

    public void LoadsceneTransition()
    {
        fader[0].gameObject.SetActive(true);
        LeanTween.scale(fader[0], Vector3.zero, 0);
        LeanTween.scale(fader[0],new Vector3(1,1,1), _toWait).setEase(LeanTweenType.clamp).setOnComplete((() =>
        {
            StartCoroutine(WaitUntilLoaded());
        }));
    }

    IEnumerator WaitUntilLoaded()
    {
        //ให้รอจนกว่าจะครบเวลาแล้วให้ object ปิดการแสดง
        yield return new WaitForSeconds(_toWait);
        fader[0].gameObject.SetActive(false);
    }
    }
}
