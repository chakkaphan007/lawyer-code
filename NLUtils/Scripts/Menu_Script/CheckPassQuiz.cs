using System;
using System.Collections;
using System.Collections.Generic;
using Naplab.NLDatabox.Script;
using UnityEngine;
using UnityEngine.UI;

public class CheckPassQuiz : NLDataBox
{
    private BoolType _SaveQuiz;

    public Button StoryButt;

    private void Update()
    {
        //ให้ปุ่มสามารถ interact ได้ถ้าผ่าน quiz มาแล้ว
        _SaveQuiz = BoolVariable(SaveQuiz);
        StoryButt.interactable = _SaveQuiz.Value;
    }
}
