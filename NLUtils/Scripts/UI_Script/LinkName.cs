using Databox;
using Fungus;
using Naplab.NLDatabox.Script;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Test
{
    public class LinkName : NLDataBox
    {
        public Flowchart _Flowchart;
        private StringType _SaveName;
        private string nameValue;

        private void Start()
        {
            //รับชื่อใน databox มา
            globalManager.GetDataboxObject(RunTimeDataBox).LoadDatabase();
            _SaveName = StringVariable(SaveName);
            Debug.Log(_SaveName.Value);
            //ให้ nameValue = SaveName ใน databox
            nameValue = _SaveName.Value;
            //ให้ string Name ใน flowchart = nameValue
            _Flowchart.SetStringVariable("Name",nameValue);
        }
    }
}
