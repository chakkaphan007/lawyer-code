using System;
using System.Collections;
using System.Collections.Generic;
using Fungus;
using Naplab.NLDatabox.Script;
using UnityEngine;
using UnityEngine.UI;

public class PopupTime : NLDataBox
{
    //ฟังชั่นแสดงตัว mission ให้เรียงลำดับกัน
    public List<GameObject> list;
    [HideInInspector] public BoolType _saveMission1;
    [HideInInspector] public BoolType _saveMission2;
    [HideInInspector] public BoolType _saveMission3;
    [HideInInspector] public BoolType _saveMission4;
    [HideInInspector] public BoolType _saveMission5;
    [HideInInspector] public BoolType _saveMission6;
    [HideInInspector] public BoolType _saveMission7;
    [HideInInspector] public BoolType _saveMission8;
    [HideInInspector] public BoolType _saveMission9;
    
    [HideInInspector] public BoolType _savePassedMission5;
    [HideInInspector] public BoolType _savePassedMission7;
    [HideInInspector] public BoolType _savePassedMission8;
    [HideInInspector] public BoolType _savePassedMission9;

    public DataboxManager_Gameplay _databox;
    private void Start()
    {
        _saveMission1 = BoolVariable(SaveMission1);
        _saveMission2 = BoolVariable(SaveMission2);
        _saveMission3 = BoolVariable(SaveMission3);
        _saveMission4 = BoolVariable(SaveMission4);
        _saveMission5 = BoolVariable(SaveMission5);
        _saveMission6 = BoolVariable(SaveMission6);
        _saveMission7 = BoolVariable(SaveMission7);
        _saveMission8 = BoolVariable(SaveMission8);
        _saveMission9 = BoolVariable(SaveMission9);
        
        _savePassedMission5 = BoolVariable(SavePassedMission5);
        _savePassedMission7 = BoolVariable(SavePassedMission7);
        _savePassedMission8 = BoolVariable(SavePassedMission8);
        _savePassedMission9 = BoolVariable(SavePassedMission9);
        SyncMissionInteract();
    }
    
    public void StartPopup()
    {
        StartCoroutine(Popup());
    }
    
    public IEnumerator Popup()
    {
        //แสดงทีละชิ้นโดยรอ 0.05 ก่อนจะแสดงอันถัดไป
        foreach (var t in list)
        {
            if (t.activeSelf)
            {
                t.GetComponent<Animator>().SetTrigger("in");
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void SyncMissionInteract()
    {
        //ตรวจสอบว่าได้ผ่าน mission ไหนมาบ้างแล้วปลดล็อค mission ที่ตรงเงื่อนไข
        if (!_saveMission2.Value && !_saveMission3.Value&&!_savePassedMission5.Value)
        {
            Flowchart.BroadcastFungusMessage("MS5Appeare");
            _saveMission5.Value = true;
        }

        if (!_saveMission4.Value&&!_savePassedMission7.Value)
        {
            Flowchart.BroadcastFungusMessage("MS7Appeare");
            _saveMission7.Value = true;
        }

        if (!_saveMission1.Value && !_saveMission3.Value &&!_saveMission5.Value&&!_savePassedMission8.Value&&_savePassedMission5.Value)
        {
            Flowchart.BroadcastFungusMessage("MS8Appeare");
            _saveMission8.Value = true;
        }

        if (!_saveMission6.Value&&!_savePassedMission9.Value)
        {
            Flowchart.BroadcastFungusMessage("MS9Appeare");
            _saveMission9.Value = true;
        }
        
        SaveGame();
        //ให้ปุ่ม mission interact ตรงกับ mission ที่ถูกปลด
        list[0].GetComponent<Button>().interactable = _saveMission1.Value;
        list[1].GetComponent<Button>().interactable = _saveMission2.Value;
        list[2].GetComponent<Button>().interactable = _saveMission3.Value;
        list[3].GetComponent<Button>().interactable = _saveMission4.Value;
        list[4].GetComponent<Button>().interactable = _saveMission5.Value;
        list[5].GetComponent<Button>().interactable = _saveMission6.Value;
        list[6].GetComponent<Button>().interactable = _saveMission7.Value;
        list[7].GetComponent<Button>().interactable = _saveMission8.Value;
        list[8].GetComponent<Button>().interactable = _saveMission9.Value;
    }
}
