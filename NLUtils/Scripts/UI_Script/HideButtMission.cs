using UnityEngine;

public class HideButtMission : MonoBehaviour
{
    public Transform butt1;
    public Transform butt2;
    public Transform butt3;
    
    public Transform Savebutt1;
    public Transform Savebutt2;
    public Transform Savebutt3; 
    
    public Transform  Resetbutt1;
    public Transform  Resetbutt2;
    public Transform  Resetbutt3;

    //เปิดปิด bookmark ตรง ms2-ms4
    public void ShowButt1()
    {
        butt1.transform.parent = Savebutt1.transform;
    }
    
    public void HideButt2()
    {
        butt1.transform.parent = Resetbutt1.transform;
        butt2.transform.parent = Savebutt2.transform;
    }
    public void HideButt3()
    {
        butt2.transform.parent = Resetbutt2.transform;
        butt3.transform.parent = Savebutt3.transform;
    }
}
