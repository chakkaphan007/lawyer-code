using UnityEngine;

namespace Naplab.NLUtils.Scripts.UI_Script
{
    
#if UNITY_EDITOR
    using UnityEditor;
#endif
    
    [ExecuteInEditMode]
    public class CameraScale : MonoBehaviour
    {
        void Start () 
        {
            //เซต Aspect ratio
            float aspect = 16f / 9f;

            float windowaspect = (float)Screen.width / (float)Screen.height;

            float rectHeight = windowaspect / aspect;
            
            Camera camera = GetComponent<Camera>();
            
            if (rectHeight < 1.0f)
            {  
                Rect cameraRect = camera.rect;

                cameraRect.width = 1.0f;
                cameraRect.height = rectHeight;
                cameraRect.x = 0;
                cameraRect.y = (1.0f - rectHeight) / 2.0f;
        
                camera.rect = cameraRect;
            }
            else
            {
                float scalewidth = 1.0f / rectHeight;

                Rect cameraRect = camera.rect;

                cameraRect.width = scalewidth;
                cameraRect.height = 1.0f;
                cameraRect.x = (1.0f - scalewidth) / 2.0f;
                cameraRect.y = 0;

                camera.rect = cameraRect;
            }
        }
    }
}
