using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Sound_Manager : MonoBehaviour
{
    private AudioSource _audio;
    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void PlaySound(AudioClip _clip)
    {
        //เล่นเสียงแบบครั้งเดียว
        _audio.PlayOneShot(_clip);
    }
}
