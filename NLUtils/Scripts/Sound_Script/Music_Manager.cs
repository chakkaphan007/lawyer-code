using System;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Menu_Script
{
public class Music_Manager : MonoBehaviour
{
    public AudioSource MusicSource;
    
    public static Music_Manager Instance = null;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad (gameObject);
    }

    private void Start()
    {
        MusicSource.Play();
    }

}
}
