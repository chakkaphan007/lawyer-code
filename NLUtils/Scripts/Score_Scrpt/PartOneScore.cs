using System;
using Databox;
using Naplab.NLDatabox.Script;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Score_Scrpt
{
    public class PartOneScore : NLDataBox
    {
    private StringType _RankMS1;
    private StringType _RankMS2;
    private StringType _RankMS3;
    private StringType _RankMS4;
    private StringType _RankMS7;
    private IntType _RetryMS1;
    private IntType _RetryMS2;
    private IntType _RetryMS3;
    private IntType _RetryMS4;
    private IntType _RetryMS7;
    
    private void LateUpdate()
    {
        _RankMS1 = StringVariable(SaveRankMS1);
        _RankMS2 = StringVariable(SaveRankMS2);
        _RankMS3 = StringVariable(SaveRankMS3);
        _RankMS4 = StringVariable(SaveRankMS4);
        _RankMS7 = StringVariable(SaveRankMS7);
        _RetryMS1 = IntVariable(SaveRetryMS1);
        _RetryMS2 = IntVariable(SaveRetryMS2);
        _RetryMS3 = IntVariable(SaveRetryMS3);
        _RetryMS4 = IntVariable(SaveRetryMS4);
        _RetryMS7 = IntVariable(SaveRetryMS7);
    }
    
    public void Mission1Pass()
    {
        if (_RetryMS1.Value >= 4)
        {
            _RetryMS1.Value = 4;
        }
        _RankMS1.Value = _RetryMS1.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS1.Value
        };
        SaveGame();
        Debug.Log(_RankMS1.Value);
    }
    public void Mission1Fail()
    {
        _RetryMS1.Value += 1;
        SaveGame();
        Debug.Log(_RetryMS1.Value);
    }
    
    public void Mission2Pass()
    {
        if (_RetryMS2.Value >= 4)
        {
            _RetryMS2.Value = 4;
        }
        _RankMS2.Value = _RetryMS2.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS2.Value
        };
        SaveGame();
    }
    public void Mission2Fail()
    {
        _RetryMS2.Value += 1;
        SaveGame();
        Debug.Log(_RetryMS2.Value);
    }
    
    public void Mission3Pass()
    {
        if (_RetryMS3.Value >= 4)
        {
            _RetryMS3.Value = 4;
        }
        _RankMS3.Value = _RetryMS3.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS3.Value
        };
        SaveGame();
    }
    
    public void Mission3Fail()
    {
        _RetryMS3.Value += 1;
        SaveGame();
    }
    
    public void Mission4Pass()
    {
        if (_RetryMS4.Value >= 4)
        {
            _RetryMS4.Value = 4;
        }
        _RankMS4.Value = _RetryMS4.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS4.Value
        };
        SaveGame();
    }
    public void Mission4Fail()
    {
        _RetryMS4.Value += 1;
        SaveGame();
    }
    public void Mission7Pass()
    {
        if (_RetryMS7.Value >= 4)
        {
            _RetryMS7.Value = 4;
        }
        _RankMS7.Value = _RetryMS7.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS7.Value
        };
        SaveGame();
    }
    public void Mission7Fail()
    {
        _RetryMS7.Value += 1;
        SaveGame();
    }
     }
}
