using System;
using Databox;
using Naplab.NLDatabox.Script;
using UnityEngine;

namespace Naplab.NLUtils.Scripts.Score_Scrpt
{
    public class PartTwoScore : NLDataBox
    {
    private StringType _RankMS1;
    private StringType _RankMS2;
    private StringType _RankMS3;
    private StringType _RankMS4;
    private StringType _RankMS5;
    private StringType _RankMS6;
    private StringType _RankMS7;
    private StringType _RankMS8;
    private StringType _RankMS9;
    private IntType _RetryMS1;
    private IntType _RetryMS2;
    private IntType _RetryMS3;
    private IntType _RetryMS4;
    private IntType _RetryMS5;
    private IntType _RetryMS6;
    private IntType _RetryMS7;
    private IntType _RetryMS8;
    private IntType _RetryMS9;
    private void LateUpdate()
    {
        _RankMS1 = StringVariable(SaveRankP2MS1);
        _RankMS2 = StringVariable(SaveRankP2MS2);
        _RankMS3 = StringVariable(SaveRankP2MS3);
        _RankMS4 = StringVariable(SaveRankP2MS4);
        _RankMS5 = StringVariable(SaveRankP2MS5);
        _RankMS6 = StringVariable(SaveRankP2MS6);
        _RankMS7 = StringVariable(SaveRankP2MS7);
        _RankMS8 = StringVariable(SaveRankP2MS8);
        _RankMS9 = StringVariable(SaveRankP2MS9);
        
        _RetryMS1 = IntVariable(SaveRetryP2MS1);
        _RetryMS2 = IntVariable(SaveRetryP2MS2);
        _RetryMS3 = IntVariable(SaveRetryP2MS3);
        _RetryMS4 = IntVariable(SaveRetryP2MS4);
        _RetryMS5 = IntVariable(SaveRetryP2MS5);
        _RetryMS6 = IntVariable(SaveRetryP2MS6);
        _RetryMS7 = IntVariable(SaveRetryP2MS7);
        _RetryMS8 = IntVariable(SaveRetryP2MS8);
        _RetryMS9 = IntVariable(SaveRetryP2MS9);
    }
    //เพิ่มเงื่อนไขเมื่อ <4 ด้วย
    public void Mission1Pass()
    {
        if (_RetryMS1.Value >= 4)
        {
            _RetryMS1.Value = 4;
        }
        _RankMS1.Value = _RetryMS1.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS1.Value
        };
        SaveGame();
        Debug.Log(_RankMS1.Value);
    }
    public void Mission1Fail()
    {
        _RetryMS1.Value += 1;
        SaveGame();
        Debug.Log(_RetryMS1.Value);
    }
    public void Mission2Pass()
    {
        if (_RetryMS2.Value >= 4)
        {
            _RetryMS2.Value = 4;
        }
        _RankMS2.Value = _RetryMS2.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS2.Value
        };
        SaveGame();
    }
    public void Mission2Fail()
    {
        _RetryMS2.Value += 1;
        SaveGame();
        Debug.Log(_RetryMS2.Value);
    }
    public void Mission3Pass()
    {
        if (_RetryMS3.Value >= 4)
        {
            _RetryMS3.Value = 4;
        }
        _RankMS3.Value = _RetryMS3.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS3.Value
        };
        SaveGame();
    }
    public void Mission3Fail()
    {
        _RetryMS3.Value += 1;
        SaveGame();
    }
    public void Mission4Pass()
    {
        if (_RetryMS4.Value >= 4)
        {
            _RetryMS4.Value = 4;
        }
        _RankMS4.Value = _RetryMS4.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS4.Value
        };
        SaveGame();
    }
    public void Mission4Fail()
    {
        _RetryMS4.Value += 1;
        SaveGame();
    }
    public void Mission5Pass()
    {
        if (_RetryMS5.Value >= 4)
        {
            _RetryMS5.Value = 4;
        }
        _RankMS5.Value = _RetryMS5.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS5.Value
        };
        SaveGame();
    }
    public void Mission5Fail()
    {
        _RetryMS5.Value += 1;
        SaveGame();
    }
    public void Mission6Pass()
    {
        if (_RetryMS6.Value >= 4)
        {
            _RetryMS6.Value = 4;
        }
        _RankMS6.Value = _RetryMS6.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS6.Value
        };
        SaveGame();
    }
    public void Mission6Fail()
    {
        _RetryMS6.Value += 1;
        SaveGame();
    }
    public void Mission7Pass()
    {
        if (_RetryMS7.Value >= 4)
        {
            _RetryMS7.Value = 4;
        }
        _RankMS7.Value = _RetryMS7.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS7.Value
        };
        SaveGame();
    }
    public void Mission7Fail()
    {
        _RetryMS7.Value += 1;
        SaveGame();
    }
    public void Mission8Pass()
    {
        if (_RetryMS8.Value >= 4)
        {
            _RetryMS8.Value = 4;
        }
        _RankMS8.Value = _RetryMS8.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS8.Value
        };
        SaveGame();
    }
    public void Mission8Fail()
    {
        _RetryMS8.Value += 1;
        SaveGame();
    }
    public void Mission9Pass()
    {
        if (_RetryMS9.Value >= 4)
        {
            _RetryMS9.Value = 4;
        }
        _RankMS9.Value = _RetryMS9.Value switch
        {
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "F",
            _ => _RankMS9.Value
        };
        SaveGame();
    }
    public void Mission9Fail()
    {
        _RetryMS9.Value += 1;
        SaveGame();
    }
     }
}
